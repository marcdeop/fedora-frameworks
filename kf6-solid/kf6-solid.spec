%global gitdate 20230922.170854
%global cmakever 5.240.0
%global commit0 5600e56baac2844aa27c89d43b133cc2e5c0f6be


%global framework solid

Name:           kf6-%{framework}
Version:        %{cmakever}^%{gitdate}.%{commit0}
Release:        129%{?dist}
Summary:        KDE Frameworks 6 Tier 1 integration module that provides hardware information
License:        LGPL-2.0-or-later
URL:            https://solid.kde.org/
Source0:        https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{commit0}.tar.gz

# Compiler Tools
BuildRequires:  bison
BuildRequires:  cmake
BuildRequires:  flex
BuildRequires:  gcc-c++

# KDE Frameworks
BuildRequires:  extra-cmake-modules

# Fedora
Requires:       kf6-filesystem
BuildRequires:  kf6-rpm-macros

# Qt
BuildRequires:  cmake(Qt6Core)
BuildRequires:  cmake(Qt6Qml)
BuildRequires:  cmake(Qt6Tools)

# Other
BuildRequires:  libupnp-devel
BuildRequires:  systemd-devel
Recommends:     media-player-info
Recommends:     udisks2
Recommends:     upower

Obsoletes:      kf6-solid-libs < 5.47.0-2
Provides:       kf6-solid-libs = %{version}-%{release}

%description
Solid provides the following features for application developers:
 - Hardware Discovery
 - Power Management
 - Network Management

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       cmake(Qt6Core)
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -n %{framework}-%{commit0} -p1

%build
%cmake_kf6 \
  -DWITH_NEW_POWER_ASYNC_API:BOOL=ON \
  -DWITH_NEW_POWER_ASYNC_FREEDESKTOP:BOOL=ON \
  -DWITH_NEW_SOLID_JOB:BOOL=ON
%cmake_build

%install
%cmake_install
%find_lang_kf6 solid6_qt
# Currently conflicts with KF5, temporary removal
rm %{buildroot}%{_bindir}/solid-power

%files -f solid6_qt.lang
%doc README.md TODO
%license LICENSES/*.txt
%{_kf6_bindir}/solid-hardware6
%{_kf6_datadir}/qlogging-categories6/%{framework}.*
%{_kf6_libdir}/libKF6Solid.so.5.240.0
%{_kf6_libdir}/libKF6Solid.so.6

%files devel
%{_kf6_archdatadir}/mkspecs/modules/qt_Solid.pri
%{_kf6_includedir}/Solid/
%{_kf6_libdir}/cmake/KF6Solid/
%{_kf6_libdir}/libKF6Solid.so

%changelog
* Sat Sep 23 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230911.192300.eaebf4a0adc65e4765ce978da1075b2682707d4f-129
- Initial Package
