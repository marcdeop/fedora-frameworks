%global gitdate 20230911.100523
%global cmakever 5.240.0
%global commit0 386f450f719aa6e20bc85f51cdbecb98cde89457
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global date 20221109
# trim changelog included in binary rpms
%global _changelog_trimtime %(date +%s -d "1 year ago")

## allow building with an older extra-cmake-modules
%global kf6_version 5.33.0

%global framework breeze-icons

Name:    breeze-icon-theme
Summary: Breeze icon theme
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 138%{?dist}

# http://techbase.kde.org/Policies/Licensing_Policy
License: LGPLv3+
URL:     https://api.kde.org/frameworks-api/frameworks-apidocs/frameworks/breeze-icons/html/

%global versiondir %(echo %{version} | cut -d. -f1-2)
Source0: https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

## upstream patches (lookaside cache)

## upstreamable patches

BuildArch: noarch

BuildRequires:  extra-cmake-modules
BuildRequires:  kf6-rpm-macros
BuildRequires:  qt6-qtbase-devel

# icon optimizations
BuildRequires: hardlink
# for optimizegraphics
#BuildRequires: kde-dev-scripts
BuildRequires: time

# inheritance, though could consider Recommends: if needed -- rex
Requires: hicolor-icon-theme

# Needed for proper Fedora logo
Requires: system-logos

# upstream name
Provides:       breeze-icons = %{version}-%{release}
Provides:       kf6-breeze-icons = %{version}-%{release}

# upgrade path, since this no longer includes cursors since 5.16.0
Obsoletes:      breeze-icon-theme < 5.17.0

%description
%{summary}.


%prep
%autosetup -n %{framework}-%{shortcommit0} -p1

%if 0%{?kf6_version:1}
sed -i -e "s|%{version}|%{kf6_version}|g" CMakeLists.txt
%endif


%build
%cmake_kf6

%cmake_build


%install
%cmake_install

# Do not use Fedora logo from upstream
rm -rf %{buildroot}%{_datadir}/icons/breeze-dark/apps/48/org.fedoraproject.AnacondaInstaller.svg
rm -rf %{buildroot}%{_datadir}/icons/breeze/apps/48/org.fedoraproject.AnacondaInstaller.svg
# Use copy found in fedora-logos
pushd %{buildroot}%{_datadir}/icons/breeze-dark/apps/48/
ln -s ../../../hicolor/48x48/apps/org.fedoraproject.AnacondaInstaller.svg org.fedoraproject.AnacondaInstaller.svg
popd
pushd %{buildroot}%{_datadir}/icons/breeze/apps/48/
ln -s ../../../hicolor/48x48/apps/org.fedoraproject.AnacondaInstaller.svg org.fedoraproject.AnacondaInstaller.svg
popd

## icon optimizations
du -s .
hardlink -c -v %{buildroot}%{_datadir}/icons/
du -s .

# %%ghost icon.cache
touch  %{buildroot}%{_kf6_datadir}/icons/{breeze,breeze-dark}/icon-theme.cache


%check

%if 0%{?fedora} > 25 || 0%{?rhel} > 7
## trigger-based scriptlets
%transfiletriggerin -- %{_datadir}/icons/breeze
gtk-update-icon-cache --force %{_datadir}/icons/breeze &>/dev/null || :

%transfiletriggerin -- %{_datadir}/icons/breeze-dark
gtk-update-icon-cache --force %{_datadir}/icons/breeze-dark &>/dev/null || :

%transfiletriggerpostun -- %{_datadir}/icons/breeze
gtk-update-icon-cache --force %{_datadir}/icons/breeze &>/dev/null || :

%transfiletriggerpostun -- %{_datadir}/icons/breeze-dark
gtk-update-icon-cache --force %{_datadir}/icons/breeze-dark &>/dev/null || :

%else
## classic scriptlets
%post
touch --no-create %{_datadir}/icons/breeze &> /dev/null || :
touch --no-create %{_datadir}/icons/breeze-dark &> /dev/null || :

%posttrans
gtk-update-icon-cache %{_datadir}/icons/breeze &> /dev/null || :
gtk-update-icon-cache %{_datadir}/icons/breeze-dark &> /dev/null || :

%postun
if [ $1 -eq 0 ] ; then
  touch --no-create %{_datadir}/icons/breeze &> /dev/null || :
  gtk-update-icon-cache %{_datadir}/icons/breeze &> /dev/null || :
  touch --no-create %{_datadir}/icons/breeze-dark &> /dev/null || :
  gtk-update-icon-cache %{_datadir}/icons/breze-dark &> /dev/null || :
fi
%endif

%files
%license COPYING-ICONS
%doc README.md
%ghost %{_datadir}/icons/breeze/icon-theme.cache
%ghost %{_datadir}/icons/breeze-dark/icon-theme.cache
%{_datadir}/icons/breeze/
%{_datadir}/icons/breeze-dark/
%{_libdir}/cmake/KF6BreezeIcons/KF6BreezeIconsConfig.cmake
%{_libdir}/cmake/KF6BreezeIcons/KF6BreezeIconsConfigVersion.cmake

%changelog
* Tue Sep 19 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230911.100523.386f450-138
- rebuilt

* Mon Sep 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230911.100523.386f450-137
- rebuilt

* Mon Sep 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230911.100523.386f450-136
- rebuilt

* Mon Sep 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230911.100523.386f450-135
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230911.100523.386f450-134
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230911.100523.386f450-133
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230911.100523.386f450-132
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230911.100523.386f450-131
- rebuilt

* Thu Sep 14 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230911.100523.386f450-130
- rebuilt

* Tue Sep 12 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230911.100523.386f450-129
- rebuilt

* Tue Sep 12 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230908.224409.e3e3fa3-128
- rebuilt

* Mon Sep 11 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230908.224409.e3e3fa3-127
- rebuilt

* Sun Sep 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230908.224409.e3e3fa3-126
- rebuilt

* Sun Sep 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230908.224409.e3e3fa3-125
- rebuilt

* Sat Sep 09 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230908.224409.e3e3fa3-124
- rebuilt

* Sat Sep 09 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230830.135849.b090ca4-123
- rebuilt

* Thu Sep 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230830.135849.b090ca4-122
- rebuilt

* Thu Sep 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230830.135849.b090ca4-121
- rebuilt

* Thu Sep 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230830.135849.b090ca4-120
- rebuilt

* Wed Sep 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230830.135849.b090ca4-119
- rebuilt

* Wed Sep 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230830.135849.b090ca4-118
- rebuilt

* Tue Sep 05 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230830.135849.b090ca4-117
- rebuilt

* Mon Sep 04 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230830.135849.b090ca4-116
- rebuilt

* Sun Sep 03 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230830.135849.b090ca4-115
- rebuilt

* Sat Sep 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230830.135849.b090ca4-114
- rebuilt

* Fri Sep 01 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230830.135849.b090ca4-113
- rebuilt

* Thu Aug 31 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230830.135849.b090ca4-112
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232642.20a0735-111
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232642.20a0735-110
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232642.20a0735-109
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232642.20a0735-108
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232642.20a0735-107
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230828.172340.759f170-106
- rebuilt

* Tue Aug 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230823.142834.11ab65f-105
- rebuilt

* Mon Aug 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230823.142834.11ab65f-104
- rebuilt

* Sun Aug 27 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230823.142834.11ab65f-103
- rebuilt

* Sat Aug 26 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230823.142834.11ab65f-102
- rebuilt

* Sat Aug 26 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230823.142834.11ab65f-101
- rebuilt

* Fri Aug 25 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230823.142834.11ab65f-100
- rebuilt

* Thu Aug 24 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230817.144649.2eb68fd-99
- rebuilt

* Wed Aug 23 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230817.144649.2eb68fd-98
- rebuilt

* Tue Aug 22 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230817.144649.2eb68fd-97
- rebuilt

* Mon Aug 21 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230817.144649.2eb68fd-96
- rebuilt

* Mon Aug 21 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230817.144649.2eb68fd-95
- rebuilt

* Sun Aug 20 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230817.144649.2eb68fd-94
- rebuilt

* Sat Aug 19 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230817.144649.2eb68fd-93
- rebuilt

* Fri Aug 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230816.194955.190ab28-92
- rebuilt

* Thu Aug 17 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.182557.ff691e1-91
- rebuilt

* Wed Aug 16 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.182557.ff691e1-90
- rebuilt

* Tue Aug 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.182557.ff691e1-89
- rebuilt

* Tue Aug 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.182557.ff691e1-88
- rebuilt

* Tue Aug 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.182557.ff691e1-87
- rebuilt

* Sun Aug 13 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.182557.ff691e1-86
- rebuilt

* Sun Aug 13 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230809.114720.260d8af-85
- rebuilt

* Fri Aug 11 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230809.114720.260d8af-84
- rebuilt

* Thu Aug 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230809.114720.260d8af-83
- rebuilt

* Thu Aug 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230809.114720.260d8af-82
- rebuilt

* Thu Aug 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230804.231306.ca69132-81
- rebuilt

* Wed Aug 09 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230804.231306.ca69132-80
- rebuilt

* Tue Aug 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230804.231306.ca69132-79
- rebuilt

* Mon Aug 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230804.231306.ca69132-78
- rebuilt

* Sun Aug 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230804.231306.ca69132-77
- rebuilt

* Sun Aug 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230804.231306.ca69132-76
- rebuilt

* Sun Aug 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230804.231306.ca69132-75
- rebuilt

* Sun Aug 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230804.231306.ca69132-74
- rebuilt

* Sat Aug 05 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230803.155355.524b028-73
- rebuilt

* Fri Aug 04 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230803.155355.524b028-72
- rebuilt

* Fri Aug 04 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230731.102242.c5770db-71
- rebuilt

* Thu Aug 03 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230731.102242.c5770db-70
- rebuilt

* Wed Aug 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230731.102242.c5770db-69
- rebuilt

* Wed Aug 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230731.102242.c5770db-68
- rebuilt

* Tue Aug 01 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230730.142234.4d8eaa5-67
- rebuilt

* Mon Jul 31 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230730.142234.4d8eaa5-66
- rebuilt

* Mon Jul 31 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230730.142234.4d8eaa5-65
- rebuilt

* Mon Jul 31 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230728.171312.be9f8fd-64
- rebuilt

* Sun Jul 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230728.171312.be9f8fd-63
- rebuilt

* Sun Jul 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230728.171312.be9f8fd-62
- rebuilt

* Sat Jul 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.145408.89136a3-61
- rebuilt

* Fri Jul 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.145408.89136a3-60
- rebuilt

* Fri Jul 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230726.094056.baaf42d-59
- rebuilt

* Thu Jul 27 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230726.094056.baaf42d-58
- rebuilt

* Thu Jul 27 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230724.230522.7bff084-57
- rebuilt

* Wed Jul 26 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230624.191423.8b4a665-56
- rebuilt

* Tue Jul 25 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230624.191423.8b4a665-55
- rebuilt

* Mon Jul 24 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230624.191423.8b4a665-54
- rebuilt

* Mon Jul 24 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230624.191423.8b4a665-53
- rebuilt

* Mon Jul 24 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230624.191423.8b4a665-52
- rebuilt

* Sat Jul 22 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230624.191423.8b4a665-51
- rebuilt

* Sat Jul 22 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230624.191423.8b4a665-50
- rebuilt

* Fri Jul 21 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230624.191423.8b4a665-49
- rebuilt

* Thu Jul 20 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230624.191423.8b4a665-48
- rebuilt

* Tue Jul 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230624.191423.8b4a665-47
- rebuilt

* Tue Jul 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230624.191423.8b4a665-46
- rebuilt

* Tue Jul 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230624.191423.8b4a665-45
- rebuilt

* Mon Jul 17 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230624.191423.8b4a665-44
- rebuilt

* Sun Jul 16 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230624.191423.8b4a665-43
- rebuilt

* Sun Jul 16 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230624.191423.8b4a665-42
- rebuilt

* Sun Jul 16 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230624.191423.8b4a665-41
- rebuilt

* Sun Jul 16 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230624.191423.8b4a665-40
- rebuilt

* Sat Jul 15 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230624.191423.8b4a665-39
- rebuilt

* Sat Jul 15 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230624.191423.8b4a665-38
- rebuilt

* Fri Jul 14 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230624.191423.8b4a665-37
- rebuilt

* Fri Jul 14 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230624.191423.8b4a665-36
- rebuilt

* Fri Jul 14 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230624.191423.8b4a665-35
- rebuilt

* Fri Jul 14 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230624.191423.8b4a665-34
- rebuilt

* Fri Jul 14 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230624.191423.8b4a665-33
- rebuilt

* Thu Jul 13 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230624.191423.8b4a665-32
- rebuilt

* Thu Jul 13 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230624.191423.8b4a665-31
- rebuilt

* Thu Jul 13 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230624.191423.8b4a665-30
- rebuilt

* Mon Jul 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230624.191423.8b4a665-29
- rebuilt

* Sun Jul 09 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230624.191423.8b4a665-28
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230624.191423.8b4a665-27
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230624.191423.8b4a665-26
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230624.191423.8b4a665-25
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230624.191423.8b4a665-24
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230624.191423.8b4a665-23
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230624.191423.8b4a665-22
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230624.191423.8b4a665-21
- rebuilt

* Thu Jul 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230624.191423.8b4a665-20
- rebuilt

* Wed Jul 05 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230624.191423.8b4a665-19
- rebuilt

* Tue Jul 04 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230624.191423.8b4a665-18
- rebuilt

* Sun Jul 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230624.191423.8b4a665-17
- rebuilt

* Sun Jul 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230624.191423.8b4a665-16
- rebuilt

* Sat Jul 01 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230518.155517.5012e7c-15
- rebuilt

* Thu Jun 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230601.155807.6269dd9-14
- rebuilt

* Wed Jun 28 2023 Justin - 5.240.0^20230601.155807.6269dd9-13
- rebuilt

* Tue Jun 27 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230601.155807.6269dd9-12
- rebuilt

* Sun Jun 25 2023 Justin - 5.240.0^20230601.155807.6269dd9-11
- rebuilt

* Sun Jun 25 2023 Justin - 5.240.0^20230601.155807.6269dd9-10
- rebuilt

* Sun Jun 25 2023 Justin - 5.240.0^20230601.155807.6269dd9-9
- rebuilt

* Sun Jun 25 2023 Justin - 5.240.0^20230601.155807.6269dd9-8
- rebuilt

* Fri Jun 23 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230601.155807.6269dd9-7
- rebuilt

* Fri Jun 02 2023 justin - 5.240.0^20230521.021709.15d6ae2-6
- rebuilt

* Fri Jun 02 2023 justin - 5.240.0^20230521.021709.41d35bd-5
- rebuilt

* Fri Jun 02 2023 justin - 5.240.0^20230521.021709.41d35bd-4
- rebuilt

* Wed May 31 2023 justin - 5.240.0^20230521.021709.41d35bd-3
- rebuilt

* Wed May 31 2023 justin - 5.240.0^20230521.021709.41d35bd-2
- rebuilt

* Fri Mar 24 2023 Justin Zobel <justin@1707.io> - 5.240.0-1
Initial KDE Frameworks 6 Build
