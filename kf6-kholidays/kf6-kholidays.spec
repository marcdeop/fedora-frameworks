%global gitdate 20230901.194437
%global cmakever 5.240.0
%global commit0 d42ac5f26b858e1ff8a50ad18f3f0a577be6738b
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global date 20221109
%undefine __cmake_in_source_build
%global framework kholidays

# uncomment to enable bootstrap mode
#global bootstrap 1

%if !0%{?bootstrap}
%global tests 1
%endif

Name:    kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 129%{?dist}
Summary: The KHolidays Library

License: LGPLv2+ and GPLv3+
URL:     https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)
Source0:  https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

BuildRequires:  extra-cmake-modules
BuildRequires:  kf6-rpm-macros

# qt6-qtbase-devel
BuildRequires:  pkgconfig(Qt6Core)
# qt6-qtdeclarative-devel
BuildRequires:  pkgconfig(Qt6Qml)
BuildRequires:  qt6-qttools-static
BuildRequires: make
#BuildRequires:  cmake(Qt6LinguistTools)

# translations moved here
Conflicts: kde-l10n < 17.03

%description
The KHolidays library provides a C++ API that determines holiday
and other special events for a geographical region.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{shortcommit0} -p1


%build
%cmake_kf6 \
  -DBUILD_TESTING:BOOL=%{?tests:ON}%{!?tests:OFF}

%cmake_build


%install
%cmake_install

%find_lang_kf6 libkholidays6_qt


%check
%if 0%{?tests}
export CTEST_OUTPUT_ON_FAILURE=1
xvfb-run -a \
dbus-launch --exit-with-session \
time \
make test ARGS="--output-on-failure --timeout 20" -C %{_target_platform} ||:
%endif


%ldconfig_scriptlets

%files -f libkholidays6_qt.lang
%license LICENSES/*.txt
%{_kf6_datadir}/qlogging-categories6/*categories
%{_kf6_libdir}/libKF6Holidays.so.*
%{_kf6_qmldir}/org/kde/kholidays/

%files devel

%{_kf6_archdatadir}/mkspecs/modules/qt_KHolidays.pri
%{_kf6_includedir}/KHolidays/
%{_kf6_libdir}/cmake/KF6Holidays/
%{_kf6_libdir}/libKF6Holidays.so


%changelog
* Tue Sep 19 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230901.194437.d42ac5f-129
- rebuilt

* Mon Sep 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230901.194437.d42ac5f-128
- rebuilt

* Mon Sep 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230901.194437.d42ac5f-127
- rebuilt

* Mon Sep 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230901.194437.d42ac5f-126
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230901.194437.d42ac5f-125
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230901.194437.d42ac5f-124
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230901.194437.d42ac5f-123
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230901.194437.d42ac5f-122
- rebuilt

* Thu Sep 14 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230901.194437.d42ac5f-121
- rebuilt

* Tue Sep 12 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230901.194437.d42ac5f-120
- rebuilt

* Tue Sep 12 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230901.194437.d42ac5f-119
- rebuilt

* Mon Sep 11 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230901.194437.d42ac5f-118
- rebuilt

* Sun Sep 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230901.194437.d42ac5f-117
- rebuilt

* Sun Sep 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230901.194437.d42ac5f-116
- rebuilt

* Sat Sep 09 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230901.194437.d42ac5f-115
- rebuilt

* Sat Sep 09 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230901.194437.d42ac5f-114
- rebuilt

* Thu Sep 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230901.194437.d42ac5f-113
- rebuilt

* Thu Sep 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230901.194437.d42ac5f-112
- rebuilt

* Thu Sep 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230901.194437.d42ac5f-111
- rebuilt

* Wed Sep 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230901.194437.d42ac5f-110
- rebuilt

* Wed Sep 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230901.194437.d42ac5f-109
- rebuilt

* Tue Sep 05 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230901.194437.d42ac5f-108
- rebuilt

* Mon Sep 04 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230901.194437.d42ac5f-107
- rebuilt

* Sun Sep 03 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230901.194437.d42ac5f-106
- rebuilt

* Sat Sep 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233046.49fd07b-105
- rebuilt

* Fri Sep 01 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233046.49fd07b-104
- rebuilt

* Thu Aug 31 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233046.49fd07b-103
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233046.49fd07b-102
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233046.49fd07b-101
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233046.49fd07b-100
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233046.49fd07b-99
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233046.49fd07b-98
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230826.121956.cb94d7d-97
- rebuilt

* Tue Aug 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230826.121956.cb94d7d-96
- rebuilt

* Mon Aug 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230826.121956.cb94d7d-95
- rebuilt

* Sun Aug 27 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.074331.bfeac88-94
- rebuilt

* Sat Aug 26 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.074331.bfeac88-93
- rebuilt

* Sat Aug 26 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.074331.bfeac88-92
- rebuilt

* Fri Aug 25 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.074331.bfeac88-91
- rebuilt

* Thu Aug 24 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.074331.bfeac88-90
- rebuilt

* Wed Aug 23 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.074331.bfeac88-89
- rebuilt

* Tue Aug 22 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.074331.bfeac88-88
- rebuilt

* Mon Aug 21 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.074331.bfeac88-87
- rebuilt

* Mon Aug 21 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.074331.bfeac88-86
- rebuilt

* Sun Aug 20 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.074331.bfeac88-85
- rebuilt

* Sat Aug 19 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.074331.bfeac88-84
- rebuilt

* Fri Aug 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.074331.bfeac88-83
- rebuilt

* Thu Aug 17 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.074331.bfeac88-82
- rebuilt

* Wed Aug 16 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.074331.bfeac88-81
- rebuilt

* Tue Aug 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.074331.bfeac88-80
- rebuilt

* Tue Aug 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.074331.bfeac88-79
- rebuilt

* Tue Aug 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.074331.bfeac88-78
- rebuilt

* Sun Aug 13 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.074331.bfeac88-77
- rebuilt

* Sun Aug 13 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230726.224525.c13cd18-76
- rebuilt

* Fri Aug 11 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230726.224525.c13cd18-75
- rebuilt

* Thu Aug 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230726.224525.c13cd18-74
- rebuilt

* Thu Aug 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230726.224525.c13cd18-73
- rebuilt

* Thu Aug 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230726.224525.c13cd18-72
- rebuilt

* Wed Aug 09 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230726.224525.c13cd18-71
- rebuilt

* Tue Aug 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230726.224525.c13cd18-70
- rebuilt

* Mon Aug 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230726.224525.c13cd18-69
- rebuilt

* Sun Aug 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230726.224525.c13cd18-68
- rebuilt

* Sun Aug 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230726.224525.c13cd18-67
- rebuilt

* Sun Aug 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230726.224525.c13cd18-66
- rebuilt

* Sat Aug 05 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230726.224525.c13cd18-65
- rebuilt

* Fri Aug 04 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230726.224525.c13cd18-64
- rebuilt

* Fri Aug 04 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230726.224525.c13cd18-63
- rebuilt

* Thu Aug 03 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230726.224525.c13cd18-62
- rebuilt

* Wed Aug 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230726.224525.c13cd18-61
- rebuilt

* Wed Aug 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230726.224525.c13cd18-60
- rebuilt

* Tue Aug 01 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230726.224525.c13cd18-59
- rebuilt

* Mon Jul 31 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230726.224525.c13cd18-58
- rebuilt

* Mon Jul 31 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230726.224525.c13cd18-57
- rebuilt

* Mon Jul 31 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230726.224525.c13cd18-56
- rebuilt

* Sun Jul 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230726.224525.c13cd18-55
- rebuilt

* Sun Jul 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230723.072810.7e03fe9-54
- rebuilt

* Sat Jul 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230723.072810.7e03fe9-53
- rebuilt

* Fri Jul 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230723.072810.7e03fe9-52
- rebuilt

* Fri Jul 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230723.072810.7e03fe9-51
- rebuilt

* Thu Jul 27 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230723.072810.7e03fe9-50
- rebuilt

* Wed Jul 26 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230723.072810.7e03fe9-49
- rebuilt

* Tue Jul 25 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230723.072810.7e03fe9-48
- rebuilt

* Mon Jul 24 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230723.072810.7e03fe9-47
- rebuilt

* Mon Jul 24 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.210109.745dbf3-46
- rebuilt

* Sat Jul 22 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.210109.745dbf3-45
- rebuilt

* Fri Jul 21 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.210109.745dbf3-44
- rebuilt

* Thu Jul 20 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.210109.745dbf3-43
- rebuilt

* Tue Jul 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.210109.745dbf3-42
- rebuilt

* Tue Jul 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.210109.745dbf3-41
- rebuilt

* Tue Jul 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.210109.745dbf3-40
- rebuilt

* Mon Jul 17 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.210109.745dbf3-39
- rebuilt

* Sun Jul 16 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230629.210109.745dbf3-38
- rebuilt

* Sun Jul 16 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230629.210109.745dbf3-37
- rebuilt

* Sat Jul 15 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230629.210109.745dbf3-36
- rebuilt

* Sat Jul 15 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230629.210109.745dbf3-35
- rebuilt

* Fri Jul 14 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230629.210109.745dbf3-34
- rebuilt

* Fri Jul 14 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230629.210109.745dbf3-33
- rebuilt

* Fri Jul 14 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230629.210109.745dbf3-32
- rebuilt

* Fri Jul 14 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230629.210109.745dbf3-31
- rebuilt

* Thu Jul 13 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230629.210109.745dbf3-30
- rebuilt

* Thu Jul 13 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230629.210109.745dbf3-29
- rebuilt

* Thu Jul 13 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230629.210109.745dbf3-28
- rebuilt

* Mon Jul 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.210109.745dbf3-27
- rebuilt

* Sun Jul 09 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230629.210109.745dbf3-26
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230629.210109.745dbf3-25
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.210109.745dbf3-24
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.210109.745dbf3-23
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.210109.745dbf3-22
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.210109.745dbf3-21
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.210109.745dbf3-20
- rebuilt

* Thu Jul 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.210109.745dbf3-19
- rebuilt

* Wed Jul 05 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.210109.745dbf3-18
- rebuilt

* Tue Jul 04 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.210109.745dbf3-17
- rebuilt

* Sun Jul 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.210109.745dbf3-16
- rebuilt

* Sun Jul 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.210109.745dbf3-15
- rebuilt

* Sat Jul 01 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230509.184552.b7ae92e-14
- rebuilt

* Thu Jun 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230509.184552.b7ae92e-13
- rebuilt

* Wed Jun 28 2023 Justin - 5.240.0^20230509.184552.b7ae92e-12
- rebuilt

* Tue Jun 27 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230509.184552.b7ae92e-11
- rebuilt

* Sun Jun 25 2023 Justin - 5.240.0^20230509.184552.b7ae92e-10
- rebuilt

* Sun Jun 25 2023 Justin - 5.240.0^20230509.184552.b7ae92e-9
- rebuilt

* Sun Jun 25 2023 Justin - 5.240.0^20230509.184552.b7ae92e-8
- rebuilt

* Fri Jun 23 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230530.030117.b7ae92e-7
- rebuilt

* Fri Jun 02 2023 justin - 5.240.0^20230530.030117.b7ae92e-6
- rebuilt

* Fri Jun 02 2023 justin - 5.240.0^20230530.030117.f91f60e-5
- rebuilt

* Fri Jun 02 2023 justin - 5.240.0^20230530.030117.f91f60e-4
- rebuilt

* Wed May 31 2023 justin - 5.240.0^20230530.030117.f91f60e-3
- rebuilt

* Wed May 31 2023 justin - 5.240.0^20230530.030117.f91f60e-2
- rebuilt

* Sat Aug 13 2022 Justin Zobel <justin@1707.io> - 5.97.0-1
- Update to 5.97.0

* Thu Jul 21 2022 Fedora Release Engineering <releng@fedoraproject.org> - 1:5.96.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_37_Mass_Rebuild

* Sun Jul 03 2022 Marc Deop <marcdeop@fedoraproject.org> - 1:5.96.0-1
- 5.96.0

* Fri May 13 2022 Rex Dieter <rdieter@fedoraproject.org> - 1:5.94.0-1
- 5.94.0

* Sun Apr 10 2022 Justin Zobel <justin@1707.io> - 5.93-1
- Update to 5.93

* Thu Mar 10 2022 Rex Dieter <rdieter@fedoraproject.org> - 1:5.92.0-1
- 5.92.0

* Fri Feb 11 2022 Rex Dieter <rdieter@fedoraproject.org> - 1:5.91.0-1
- 5.91.0

* Thu Jan 20 2022 Fedora Release Engineering <releng@fedoraproject.org> - 1:5.90.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_36_Mass_Rebuild

* Tue Jan 04 2022 Marc Deop i Argemí (Private) <marc@marcdeop.com> - 1:5.90.0-1
- 5.90.0

* Wed Dec 08 2021 Rex Dieter <rdieter@fedoraproject.org> - 1:5.89.0-1
- 5.89.0

* Mon Nov 08 2021 Marc Deop <marcdeop@fedoraproject.org> - 1:5.88.0-1
- 5.88.0

* Tue Oct 05 2021 Rex Dieter <rdieter@fedoraproject.org> - 1:5.87.0-1
- 5.87.0

* Tue Sep 14 2021 Marc Deop <marcdeop@fedoraproject.org> - 1:5.86.0-1
- 5.86.0

* Thu Aug 12 2021 Rex Dieter <rdieter@fedoraproject.org> - 1:5.85.0-1
- 5.85.0

* Thu Jul 22 2021 Fedora Release Engineering <releng@fedoraproject.org> - 1:5.83.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_35_Mass_Rebuild

* Tue Jun 08 2021 Rex Dieter <rdieter@fedoraproject.org> - 1:5.83.0-1
- 5.83.0

* Mon May 03 2021 Rex Dieter <rdieter@fedoraproject.org> - 1:5.82.0-1
- 5.82.0

* Tue Apr 06 2021 Rex Dieter <rdieter@fedoraproject.org> - 1:5.81.0-1
- 5.81.0

* Tue Mar 09 2021 Rex Dieter <rdieter@fedoraproject.org> - 1:5.80.0-1
- 5.80.0

* Sat Feb 06 2021 Rex Dieter <rdieter@fedoraproject.org> - 1:5.79.0-2
- respin

* Sat Feb 06 2021 Rex Dieter <rdieter@fedoraproject.org> - 1:5.79.0-1
- 5.79.0

* Tue Jan 26 2021 Fedora Release Engineering <releng@fedoraproject.org> - 1:5.78.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_34_Mass_Rebuild

* Mon Jan  4 08:47:00 CST 2021 Rex Dieter <rdieter@fedoraproject.org> - 1:5.78.0-1
- 5.78.0

* Sun Dec 13 14:13:36 CST 2020 Rex Dieter <rdieter@fedoraproject.org> - 1:5.77.0-1
- 5.77.0

* Thu Nov 19 09:02:46 CST 2020 Rex Dieter <rdieter@fedoraproject.org> - 1:5.76.0-1
- 5.76.0

* Wed Oct 14 09:55:03 CDT 2020 Rex Dieter <rdieter@fedoraproject.org> - 1:5.75.0-1
- 5.75.0

* Fri Sep 18 2020 Jan Grulich <jgrulich@redhat.com> - 1:5.74.0-1
- 5.74.0

* Mon Aug 03 2020 Rex Dieter <rdieter@fedoraproject.org> - 1:5.73.0-1
- 5.73.0

* Tue Jul 28 2020 Fedora Release Engineering <releng@fedoraproject.org> - 1:5.72.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_33_Mass_Rebuild

* Tue Jul 07 2020 Rex Dieter <rdieter@fedoraproject.org> - 1:5.72.0-1
- 5.72.0

* Tue Jun 16 2020 Rex Dieter <rdieter@fedoraproject.org> - 1:5.71.0-1
- 5.71.0

* Mon May 04 2020 Rex Dieter <rdieter@fedoraproject.org> - 1:5.70.0-1
- 5.70.0

* Tue Apr 21 2020 Rex Dieter <rdieter@fedoraproject.org> - 1:5.69.0-1
- 5.69.0

* Fri Mar 20 2020 Rex Dieter <rdieter@fedoraproject.org> - 1:5.68.0-1
- 5.68.0

* Mon Feb 03 2020 Rex Dieter <rdieter@fedoraproject.org> - 1:5.67.0-1
- 5.67.0

* Wed Jan 29 2020 Fedora Release Engineering <releng@fedoraproject.org> - 1:5.66.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_32_Mass_Rebuild

* Tue Jan 07 2020 Rex Dieter <rdieter@fedoraproject.org> - 1:5.66.0-1
- 5.66.0

* Tue Dec 17 2019 Rex Dieter <rdieter@fedoraproject.org> - 1:5.65.0-1
- 5.65.0

* Fri Nov 08 2019 Rex Dieter <rdieter@fedoraproject.org> - 1:5.64.0-1
- 5.64.0

* Tue Oct 22 2019 Rex Dieter <rdieter@fedoraproject.org> - 1:5.63.0-1
- 5.63.0

* Mon Sep 16 2019 Rex Dieter <rdieter@fedoraproject.org> - 1:5.62.0-1
- 5.62.0

* Wed Aug 07 2019 Rex Dieter <rdieter@fedoraproject.org> - 1:5.61.0-1
- 5.61.0

* Thu Jul 25 2019 Fedora Release Engineering <releng@fedoraproject.org> - 1:5.60.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_31_Mass_Rebuild

* Sat Jul 13 2019 Rex Dieter <rdieter@fedoraproject.org> - 1:5.60.0-1
- 5.60.0

* Thu Jun 06 2019 Rex Dieter <rdieter@fedoraproject.org> - 1:5.59.0-1
- 5.59.0

* Tue May 07 2019 Rex Dieter <rdieter@fedoraproject.org> - 1:5.58.0-1
- 5.58.0

* Tue Apr 09 2019 Rex Dieter <rdieter@fedoraproject.org> - 1:5.57.0-1
- 5.57.0

* Tue Mar 05 2019 Rex Dieter <rdieter@fedoraproject.org> - 1:5.56.0-1
- 5.56.0

* Mon Feb 04 2019 Rex Dieter <rdieter@fedoraproject.org> - 1:5.55.0-1
- 5.55.0

* Fri Feb 01 2019 Fedora Release Engineering <releng@fedoraproject.org> - 1:5.54.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_30_Mass_Rebuild

* Tue Jan 08 2019 Rex Dieter <rdieter@fedoraproject.org> - 1:5.54.0-1
- 5.54.0

* Sun Dec 09 2018 Rex Dieter <rdieter@fedoraproject.org> - 1:5.53.0-1
- 5.53.0

* Sun Nov 04 2018 Rex Dieter <rdieter@fedoraproject.org> - 1:5.52.0-1
- 5.52.0

* Wed Oct 10 2018 Rex Dieter <rdieter@fedoraproject.org> - 1:5.51.0-1
- 5.51.0

* Tue Sep 04 2018 Rex Dieter <rdieter@fedoraproject.org> - 1:5.50.0-1
- 5.50.0

* Tue Aug 07 2018 Rex Dieter <rdieter@fedoraproject.org> - 1:5.49.0-1
- 5.49.0

* Fri Jul 13 2018 Fedora Release Engineering <releng@fedoraproject.org> - 1:5.48.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Mon Jul 09 2018 Rex Dieter <rdieter@fedoraproject.org> - 1:5.48.0-1
- 5.48.0

* Sat Jun 02 2018 Rex Dieter <rdieter@fedoraproject.org> - 1:5.47.0-1
- 5.47.0

* Sat May 05 2018 Rex Dieter <rdieter@fedoraproject.org> - 1:5.46.0-1
- 5.46.0

* Sun Apr 08 2018 Rex Dieter <rdieter@fedoraproject.org> - 1:5.45.0-1
- 5.45.0

* Sun Mar 04 2018 Rex Dieter <rdieter@fedoraproject.org> - 1:5.44.0-1
- kholidays-5.44.0 (part of kde frameworks now), epoch++
- use %%make_build %%ldconfig_scriptlets

* Tue Feb 06 2018 Rex Dieter <rdieter@fedoraproject.org> - 17.12.2-1
- 17.12.2

* Thu Jan 11 2018 Rex Dieter <rdieter@fedoraproject.org> - 17.12.1-1
- 17.12.1

* Tue Dec 12 2017 Rex Dieter <rdieter@fedoraproject.org> - 17.12.0-1
- 17.12.0

* Wed Dec 06 2017 Rex Dieter <rdieter@fedoraproject.org> - 17.11.90-1
- 17.11.90

* Wed Nov 22 2017 Rex Dieter <rdieter@fedoraproject.org> - 17.11.80-1
- 17.11.80

* Wed Nov 08 2017 Rex Dieter <rdieter@fedoraproject.org> - 17.08.3-1
- 17.08.3

* Mon Sep 25 2017 Rex Dieter <rdieter@fedoraproject.org> - 17.08.1-1
- 17.08.1

* Fri Jul 28 2017 Rex Dieter <rdieter@fedoraproject.org> - 17.04.3-1
- 17.04.3

* Wed Jul 26 2017 Fedora Release Engineering <releng@fedoraproject.org> - 17.04.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Thu Jun 15 2017 Rex Dieter <rdieter@fedoraproject.org> - 17.04.2-1
- 17.04.2

* Thu May 11 2017 Rex Dieter <rdieter@fedoraproject.org> - 17.04.1-1
- 17.04.1

* Thu Mar 09 2017 Rex Dieter <rdieter@fedoraproject.org> - 16.12.3-1
- 16.12.3

* Thu Feb 09 2017 Rex Dieter <rdieter@fedoraproject.org> - 16.12.2-1
- 16.12.2

* Mon Jan 16 2017 Rex Dieter <rdieter@fedoraproject.org> - 16.12.1-1
- 16.12.1

* Mon Dec 05 2016 Rex Dieter <rdieter@fedoraproject.org> - 16.08.3-1
- 16.08.3

* Thu Oct 13 2016 Rex Dieter <rdieter@fedoraproject.org> - 16.08.2-1
- 16.08.2

* Thu Sep 08 2016 Rex Dieter <rdieter@fedoraproject.org> - 16.08.1-1
- 16.08.1

* Sat Sep 03 2016 Rex Dieter <rdieter@fedoraproject.org> - 16.08.0-1
- 16.08.0

* Sun Jul 10 2016 Rex Dieter <rdieter@fedoraproject.org> - 16.04.3-1
- 16.04.3

* Sun Jun 12 2016 Rex Dieter <rdieter@fedoraproject.org> - 16.04.2-1
- 16.04.2

* Sun May 08 2016 Rex Dieter <rdieter@fedoraproject.org> - 16.04.1-1
- 16.04.1

* Sun May 01 2016 Rex Dieter <rdieter@fedoraproject.org> - 16.04.0-1
- 16.04.0, update URL, support bootstrap, add %%check

* Tue Mar 15 2016 Rex Dieter <rdieter@fedoraproject.org> - 15.12.3-1
- 15.12.3

* Sun Feb 14 2016 Rex Dieter <rdieter@fedoraproject.org> - 15.12.2-1
- 15.12.2

* Thu Feb 11 2016 Rex Dieter <rdieter@fedoraproject.org> 15.12.1-1
- 15.12.1
- fix gcc6 FTBFS

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 15.12.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Tue Dec 15 2015 Jan Grulich <jgrulich@redhat.com> - 15.12.0-2
- Remove obsoletes/conflicts with kdepimlibs

* Tue Dec 15 2015 Jan Grulich <jgrulich@redhat.com> - 15.12-0-1
- Update to 15.12.0

* Mon Dec 07 2015 Jan Grulich <jgrulich@redhat.com> - 15.11.90-1
- Update to 15.11.90

* Thu Dec 03 2015 Jan Grulich <jgrulich@redhat.com> - 15.11.80-1
- Update to 15.11.80

* Mon Aug 24 2015 Daniel Vrátil <dvratil@redhat.com> - 15.08.0-1
- Initial version
