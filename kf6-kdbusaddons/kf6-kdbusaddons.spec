%global gitdate 20230829.232927
%global cmakever 5.240.0
%global commit0 fbb85588c24d0f118959c530022cab2833cf5a39
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global date 20221109
%global framework kdbusaddons

Name:    kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 130%{?dist}
Summary: KDE Frameworks 6 Tier 1 addon with various classes on top of QtDBus

License: LGPLv2+
URL:     https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)
Source0:  https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

BuildRequires:  extra-cmake-modules
BuildRequires:  kf6-rpm-macros

BuildRequires:  qt6-qtbase-devel
BuildRequires:  qt6-qtbase-private-devel
BuildRequires:  qt6-qttools-devel
#BuildRequires:  qt6-qtx11extras-devel

BuildRequires:  pkgconfig(xkbcommon)

Requires:       kf6-filesystem

%description
KDBusAddons provides convenience classes on top of QtDBus, as well as an API to
create KDED modules.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       qt6-qtbase-devel
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{shortcommit0} -p1


%build
%cmake_kf6

%cmake_build


%install
%cmake_install

%find_lang_kf6 kdbusaddons6_qt


%ldconfig_scriptlets

%files -f kdbusaddons6_qt.lang
%doc README.md
%license LICENSES/*.txt
%{_kf6_datadir}/qlogging-categories6/%{framework}*
%{_kf6_bindir}/kquitapp6
%{_kf6_libdir}/libKF6DBusAddons.so.*

%files devel
%{_kf6_includedir}/KDBusAddons/
%{_kf6_libdir}/libKF6DBusAddons.so
%{_kf6_libdir}/cmake/KF6DBusAddons/
%{_kf6_archdatadir}/mkspecs/modules/qt_KDBusAddons.pri


%changelog
* Tue Sep 19 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-130
- rebuilt

* Mon Sep 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-129
- rebuilt

* Mon Sep 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-128
- rebuilt

* Mon Sep 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-127
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-126
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-125
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-124
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-123
- rebuilt

* Thu Sep 14 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-122
- rebuilt

* Tue Sep 12 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-121
- rebuilt

* Tue Sep 12 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-120
- rebuilt

* Mon Sep 11 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-119
- rebuilt

* Sun Sep 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-118
- rebuilt

* Sun Sep 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-117
- rebuilt

* Sat Sep 09 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-116
- rebuilt

* Sat Sep 09 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-115
- rebuilt

* Thu Sep 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-114
- rebuilt

* Thu Sep 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-113
- rebuilt

* Thu Sep 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-112
- rebuilt

* Wed Sep 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-111
- rebuilt

* Wed Sep 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-110
- rebuilt

* Tue Sep 05 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-109
- rebuilt

* Mon Sep 04 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-108
- rebuilt

* Sun Sep 03 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-107
- rebuilt

* Sat Sep 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-106
- rebuilt

* Fri Sep 01 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-105
- rebuilt

* Thu Aug 31 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-104
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-103
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-102
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-101
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-100
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232927.fbb8558-99
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.154702.d15677a-98
- rebuilt

* Tue Aug 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.154702.d15677a-97
- rebuilt

* Mon Aug 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.154702.d15677a-96
- rebuilt

* Sun Aug 27 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.154702.d15677a-95
- rebuilt

* Sat Aug 26 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.154702.d15677a-94
- rebuilt

* Sat Aug 26 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.154702.d15677a-93
- rebuilt

* Fri Aug 25 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.154702.d15677a-92
- rebuilt

* Thu Aug 24 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.154702.d15677a-91
- rebuilt

* Wed Aug 23 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.154702.d15677a-90
- rebuilt

* Tue Aug 22 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.154702.d15677a-89
- rebuilt

* Mon Aug 21 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.154702.d15677a-88
- rebuilt

* Mon Aug 21 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.154702.d15677a-87
- rebuilt

* Sun Aug 20 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.154702.d15677a-86
- rebuilt

* Sat Aug 19 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.154702.d15677a-85
- rebuilt

* Fri Aug 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.154702.d15677a-84
- rebuilt

* Thu Aug 17 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.154702.d15677a-83
- rebuilt

* Wed Aug 16 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.154702.d15677a-82
- rebuilt

* Tue Aug 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.154702.d15677a-81
- rebuilt

* Tue Aug 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.154702.d15677a-80
- rebuilt

* Tue Aug 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230812.154702.d15677a-79
- rebuilt

* Sun Aug 13 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154627.cf99003-78
- rebuilt

* Sun Aug 13 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154627.cf99003-77
- rebuilt

* Fri Aug 11 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154627.cf99003-76
- rebuilt

* Thu Aug 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154627.cf99003-75
- rebuilt

* Thu Aug 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154627.cf99003-74
- rebuilt

* Thu Aug 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154627.cf99003-73
- rebuilt

* Wed Aug 09 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154627.cf99003-72
- rebuilt

* Tue Aug 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154627.cf99003-71
- rebuilt

* Mon Aug 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154627.cf99003-70
- rebuilt

* Sun Aug 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154627.cf99003-69
- rebuilt

* Sun Aug 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154627.cf99003-68
- rebuilt

* Sun Aug 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154627.cf99003-67
- rebuilt

* Sun Aug 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154627.cf99003-66
- rebuilt

* Sat Aug 05 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154627.cf99003-65
- rebuilt

* Fri Aug 04 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154627.cf99003-64
- rebuilt

* Fri Aug 04 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154627.cf99003-63
- rebuilt

* Thu Aug 03 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154627.cf99003-62
- rebuilt

* Wed Aug 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154627.cf99003-61
- rebuilt

* Wed Aug 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-60
- rebuilt

* Tue Aug 01 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-59
- rebuilt

* Mon Jul 31 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-58
- rebuilt

* Mon Jul 31 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-57
- rebuilt

* Mon Jul 31 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-56
- rebuilt

* Sun Jul 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-55
- rebuilt

* Sun Jul 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-54
- rebuilt

* Sat Jul 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-53
- rebuilt

* Fri Jul 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-52
- rebuilt

* Fri Jul 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-51
- rebuilt

* Thu Jul 27 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-50
- rebuilt

* Wed Jul 26 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-49
- rebuilt

* Tue Jul 25 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-48
- rebuilt

* Mon Jul 24 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-47
- rebuilt

* Mon Jul 24 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-46
- rebuilt

* Sat Jul 22 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-45
- rebuilt

* Fri Jul 21 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-44
- rebuilt

* Thu Jul 20 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-43
- rebuilt

* Tue Jul 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-42
- rebuilt

* Tue Jul 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-41
- rebuilt

* Tue Jul 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-40
- rebuilt

* Mon Jul 17 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-39
- rebuilt

* Sun Jul 16 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230630.234536.d287d57-38
- rebuilt

* Sun Jul 16 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230630.234536.d287d57-37
- rebuilt

* Sat Jul 15 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230630.234536.d287d57-36
- rebuilt

* Sat Jul 15 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230630.234536.d287d57-35
- rebuilt

* Fri Jul 14 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230630.234536.d287d57-34
- rebuilt

* Fri Jul 14 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230630.234536.d287d57-33
- rebuilt

* Fri Jul 14 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230630.234536.d287d57-32
- rebuilt

* Fri Jul 14 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230630.234536.d287d57-31
- rebuilt

* Thu Jul 13 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230630.234536.d287d57-30
- rebuilt

* Thu Jul 13 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230630.234536.d287d57-29
- rebuilt

* Thu Jul 13 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230630.234536.d287d57-28
- rebuilt

* Mon Jul 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-27
- rebuilt

* Sun Jul 09 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230630.234536.d287d57-26
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230630.234536.d287d57-25
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-24
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-23
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-22
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-21
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-20
- rebuilt

* Thu Jul 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-19
- rebuilt

* Wed Jul 05 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-18
- rebuilt

* Tue Jul 04 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-17
- rebuilt

* Sun Jul 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-16
- rebuilt

* Sun Jul 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.234536.d287d57-15
- rebuilt

* Sat Jul 01 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230627.221239.e91a110-14
- rebuilt

* Thu Jun 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230627.221239.e91a110-13
- rebuilt

* Wed Jun 28 2023 Justin - 5.240.0^20210717.183356.274b2f7-12
- rebuilt

* Tue Jun 27 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20210717.183356.274b2f7-11
- rebuilt

* Sun Jun 25 2023 Justin - 5.240.0^20210717.183356.274b2f7-10
- rebuilt

* Sun Jun 25 2023 Justin - 5.240.0^20210717.183356.274b2f7-9
- rebuilt

* Sun Jun 25 2023 Justin - 5.240.0^20210717.183356.274b2f7-8
- rebuilt

* Fri Jun 23 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230530.030013.274b2f7-7
- rebuilt

* Fri Jun 02 2023 justin - 5.240.0^20230530.030013.274b2f7-6
- rebuilt

* Fri Jun 02 2023 justin - 5.240.0^20230530.030013.53066bf-5
- rebuilt

* Fri Jun 02 2023 justin - 5.240.0^20230530.030013.53066bf-4
- rebuilt

* Wed May 31 2023 justin - 5.240.0^20230530.030013.53066bf-3
- rebuilt

* Wed May 31 2023 justin - 5.240.0^20230530.030013.53066bf-2
- rebuilt

* Sat Aug 13 2022 Justin Zobel <justin@1707.io> - 5.97.0-1
- Update to 5.97.0

* Thu Jul 21 2022 Fedora Release Engineering <releng@fedoraproject.org> - 5.96.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_37_Mass_Rebuild

* Sun Jul 03 2022 Marc Deop <marcdeop@fedoraproject.org> - 5.96.0-1
- 5.96.0

* Fri May 13 2022 Rex Dieter <rdieter@fedoraproject.org> - 5.94.0-1
- 5.94.0

* Sun Apr 10 2022 Justin Zobel <justin@1707.io> - 5.93-1
- Update to 5.93

* Thu Mar 10 2022 Rex Dieter <rdieter@fedoraproject.org> - 5.92.0-1
- 5.92.0

* Fri Feb 11 2022 Rex Dieter <rdieter@fedoraproject.org> - 5.91.0-1
- 5.91.0

* Thu Jan 20 2022 Fedora Release Engineering <releng@fedoraproject.org> - 5.90.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_36_Mass_Rebuild

* Tue Jan 04 2022 Marc Deop i Argemí (Private) <marc@marcdeop.com> - 5.90.0-1
- 5.90.0

* Wed Dec 08 2021 Rex Dieter <rdieter@fedoraproject.org> - 5.89.0-1
- 5.89.0

* Mon Nov 08 2021 Marc Deop <marcdeop@fedoraproject.org> - 5.88.0-1
- 5.88.0

* Tue Oct 05 2021 Rex Dieter <rdieter@fedoraproject.org> - 5.87.0-1
- 5.87.0

* Tue Sep 14 2021 Marc Deop <marcdeop@fedoraproject.org> - 5.86.0-1
- 5.86.0

* Thu Aug 12 2021 Rex Dieter <rdieter@fedoraproject.org> - 5.85.0-1
- 5.85.0

* Thu Jul 22 2021 Fedora Release Engineering <releng@fedoraproject.org> - 5.83.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_35_Mass_Rebuild

* Tue Jun 08 2021 Rex Dieter <rdieter@fedoraproject.org> - 5.83.0-1
- 5.83.0

* Mon May 03 2021 Rex Dieter <rdieter@fedoraproject.org> - 5.82.0-1
- 5.82.0

* Tue Apr 06 2021 Rex Dieter <rdieter@fedoraproject.org> - 5.81.0-1
- 5.81.0

* Tue Mar 09 2021 Rex Dieter <rdieter@fedoraproject.org> - 5.80.0-1
- 5.80.0

* Sat Feb 06 2021 Rex Dieter <rdieter@fedoraproject.org> - 5.79.0-2
- respin

* Sat Feb 06 2021 Rex Dieter <rdieter@fedoraproject.org> - 5.79.0-1
- 5.79.0

* Tue Jan 26 2021 Fedora Release Engineering <releng@fedoraproject.org> - 5.78.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_34_Mass_Rebuild

* Mon Jan  4 08:43:34 CST 2021 Rex Dieter <rdieter@fedoraproject.org> - 5.78.0-1
- 5.78.0

* Sun Dec 13 14:10:48 CST 2020 Rex Dieter <rdieter@fedoraproject.org> - 5.77.0-1
- 5.77.0

* Thu Nov 19 08:59:04 CST 2020 Rex Dieter <rdieter@fedoraproject.org> - 5.76.0-1
- 5.76.0

* Wed Oct 14 09:50:22 CDT 2020 Rex Dieter <rdieter@fedoraproject.org> - 5.75.0-1
- 5.75.0

* Fri Sep 18 2020 Jan Grulich <jgrulich@redhat.com> - 5.74.0-1
- 5.74.0

* Mon Aug 03 2020 Rex Dieter <rdieter@fedoraproject.org> - 5.73.0-1
- 5.73.0

* Tue Jul 28 2020 Fedora Release Engineering <releng@fedoraproject.org> - 5.72.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_33_Mass_Rebuild

* Tue Jul 07 2020 Rex Dieter <rdieter@fedoraproject.org> - 5.72.0-1
- 5.72.0

* Tue Jun 16 2020 Rex Dieter <rdieter@fedoraproject.org> - 5.71.0-1
- 5.71.0

* Mon May 04 2020 Rex Dieter <rdieter@fedoraproject.org> - 5.70.0-1
- 5.70.0

* Tue Apr 21 2020 Rex Dieter <rdieter@fedoraproject.org> - 5.69.0-1
- 5.69.0

* Fri Mar 20 2020 Rex Dieter <rdieter@fedoraproject.org> - 5.68.0-1
- 5.68.0

* Sun Feb 02 2020 Rex Dieter <rdieter@fedoraproject.org> - 5.67.0-1
- 5.67.0

* Wed Jan 29 2020 Fedora Release Engineering <releng@fedoraproject.org> - 5.66.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_32_Mass_Rebuild

* Tue Jan 07 2020 Rex Dieter <rdieter@fedoraproject.org> - 5.66.0-1
- 5.66.0

* Tue Dec 17 2019 Rex Dieter <rdieter@fedoraproject.org> - 5.65.0-1
- 5.65.0

* Fri Nov 08 2019 Rex Dieter <rdieter@fedoraproject.org> - 5.64.0-1
- 5.64.0

* Tue Oct 22 2019 Rex Dieter <rdieter@fedoraproject.org> - 5.63.0-1
- 5.63.0

* Mon Sep 16 2019 Rex Dieter <rdieter@fedoraproject.org> - 5.62.0-1
- 5.62.0

* Wed Aug 07 2019 Rex Dieter <rdieter@fedoraproject.org> - 5.61.0-1
- 5.61.0

* Thu Jul 25 2019 Fedora Release Engineering <releng@fedoraproject.org> - 5.60.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_31_Mass_Rebuild

* Sat Jul 13 2019 Rex Dieter <rdieter@fedoraproject.org> - 5.60.0-1
- 5.60.0

* Thu Jun 06 2019 Rex Dieter <rdieter@fedoraproject.org> - 5.59.0-1
- 5.59.0

* Tue May 07 2019 Rex Dieter <rdieter@fedoraproject.org> - 5.58.0-1
- 5.58.0

* Tue Apr 09 2019 Rex Dieter <rdieter@fedoraproject.org> - 5.57.0-1
- 5.57.0

* Tue Mar 05 2019 Rex Dieter <rdieter@fedoraproject.org> - 5.56.0-1
- 5.56.0

* Mon Feb 04 2019 Rex Dieter <rdieter@fedoraproject.org> - 5.55.0-1
- 5.55.0

* Fri Feb 01 2019 Fedora Release Engineering <releng@fedoraproject.org> - 5.54.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_30_Mass_Rebuild

* Tue Jan 08 2019 Rex Dieter <rdieter@fedoraproject.org> - 5.54.0-1
- 5.54.0

* Sun Dec 09 2018 Rex Dieter <rdieter@fedoraproject.org> - 5.53.0-1
- 5.53.0

* Sun Nov 04 2018 Rex Dieter <rdieter@fedoraproject.org> - 5.52.0-1
- 5.52.0

* Wed Oct 10 2018 Rex Dieter <rdieter@fedoraproject.org> - 5.51.0-1
- 5.51.0

* Tue Sep 04 2018 Rex Dieter <rdieter@fedoraproject.org> - 5.50.0-1
- 5.50.0

* Tue Aug 07 2018 Rex Dieter <rdieter@fedoraproject.org> - 5.49.0-1
- 5.49.0

* Fri Jul 13 2018 Fedora Release Engineering <releng@fedoraproject.org> - 5.48.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Mon Jul 09 2018 Rex Dieter <rdieter@fedoraproject.org> - 5.48.0-1
- 5.48.0

* Thu Jun 07 2018 Rex Dieter <rdieter@fedoraproject.org> - 5.47.0-2
- cleanup, use %%majmin %%make_build %%find_lang_kf6 %%ldconfig_scriptlets

* Sat Jun 02 2018 Rex Dieter <rdieter@fedoraproject.org> - 5.47.0-1
- 5.47.0

* Sat May 05 2018 Rex Dieter <rdieter@fedoraproject.org> - 5.46.0-1
- 5.46.0

* Sun Apr 08 2018 Rex Dieter <rdieter@fedoraproject.org> - 5.45.0-1
- 5.45.0

* Sat Mar 03 2018 Rex Dieter <rdieter@fedoraproject.org> - 5.44.0-1
- 5.44.0

* Wed Feb 07 2018 Rex Dieter <rdieter@fedoraproject.org> - 5.43.0-1
- 5.43.0

* Mon Jan 08 2018 Rex Dieter <rdieter@fedoraproject.org> - 5.42.0-1
- 5.42.0

* Mon Dec 04 2017 Rex Dieter <rdieter@fedoraproject.org> - 5.41.0-1
- 5.41.0

* Fri Nov 10 2017 Rex Dieter <rdieter@fedoraproject.org> - 5.40.0-1
- 5.40.0

* Sun Oct 08 2017 Rex Dieter <rdieter@fedoraproject.org> - 5.39.0-1
- 5.39.0

* Mon Sep 11 2017 Rex Dieter <rdieter@fedoraproject.org> - 5.38.0-1
- 5.38.0

* Fri Aug 25 2017 Rex Dieter <rdieter@fedoraproject.org> - 5.37.0-1
- 5.37.0

* Thu Aug 03 2017 Fedora Release Engineering <releng@fedoraproject.org> - 5.36.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Wed Jul 26 2017 Fedora Release Engineering <releng@fedoraproject.org> - 5.36.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Mon Jul 03 2017 Rex Dieter <rdieter@fedoraproject.org> - 5.36.0-1
- 5.36.0

* Sun Jun 04 2017 Rex Dieter <rdieter@fedoraproject.org> - 5.35.0-1
- 5.35.0

* Mon May 15 2017 Rex Dieter <rdieter@fedoraproject.org> - 5.34.0-1
- 5.34.0

* Mon Apr 03 2017 Rex Dieter <rdieter@fedoraproject.org> - 5.33.0-1
- 5.33.0

* Sat Mar 04 2017 Rex Dieter <rdieter@fedoraproject.org> - 5.32.0-1
- 5.32.0

* Mon Feb 06 2017 Rex Dieter <rdieter@fedoraproject.org> - 5.31.0-1
- 5.31.0

* Fri Dec 16 2016 Rex Dieter <rdieter@fedoraproject.org> - 5.29.0-1
- 5.29.0

* Fri Dec 16 2016 Rex Dieter <rdieter@fedoraproject.org> - 5.29.0-1
- 5.29.0

* Mon Oct 03 2016 Rex Dieter <rdieter@fedoraproject.org> - 5.27.0-1
- 5.27.0

* Wed Sep 07 2016 Daniel Vrátil <dvratil@fedoraproject.org> - 5.26.0-1
- KDE Frameworks 6.26.0

* Mon Aug 08 2016 Daniel Vrátil <dvratil@fedoraproject.org> - 5.25.0-1
- KDE Frameworks 6.25.0

* Wed Jul 06 2016 Daniel Vrátil <dvratil@fedoraproject.org> - 5.24.0-1
- KDE Frameworks 6.24.0

* Tue Jun 07 2016 Daniel Vrátil <dvratil@fedoraproject.org> - 5.23.0-1
- KDE Frameworks 6.23.0

* Mon May 16 2016 Rex Dieter <rdieter@fedoraproject.org> - 5.22.0-1
- KDE Frameworks 6.22.0

* Mon May 16 2016 Rex Dieter <rdieter@fedoraproject.org> - 5.21.0-2
- update URL, use autosetup

* Mon Apr 04 2016 Rex Dieter <rdieter@fedoraproject.org> - 5.21.0-1
- KDE Frameworks 6.21.0

* Mon Mar 14 2016 Daniel Vrátil <dvratil@fedoraproject.org> - 5.20.0-1
- KDE Frameworks 6.20.0

* Thu Feb 11 2016 Daniel Vrátil <dvratil@fedoraproject.org> - 5.19.0-1
- KDE Frameworks 6.19.0

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 5.18.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Thu Jan 14 2016 Rex Dieter <rdieter@fedoraproject.org> 5.18.0-2
- cosmetics, update URL, use %%license

* Sun Jan 03 2016 Daniel Vrátil <dvratil@fedoraproject.org> - 5.18.0-1
- KDE Frameworks 6.18.0

* Tue Dec 08 2015 Daniel Vrátil <dvratil@fedoraproject.org> - 5.17.0-1
- KDE Frameworks 6.17.0

* Sun Nov 08 2015 Daniel Vrátil <dvratil@fedoraproject.org> - 5.16.0-1
- KDE Frameworks 6.16.0

* Thu Oct 08 2015 Daniel Vrátil <dvratil@redhat.com> - 5.15.0-1
- KDE Frameworks 6.15.0

* Wed Sep 16 2015 Daniel Vrátil <dvratil@redhat.com> - 5.14.0-1
- KDE Frameworks 6.14.0

* Wed Aug 19 2015 Daniel Vrátil <dvratil@redhat.com> - 5.13.0-1
- KDE Frameworks 6.13.0

* Wed Aug 19 2015 Daniel Vrátil <dvratil@redhat.com> - 5.13.0-1
- KDE Frameworks 6.13.0

* Tue Aug 11 2015 Daniel Vrátil <dvratil@redhat.com> - 5.13.0-0.1
- KDE Frameworks 6.13

* Thu Jul 09 2015 Rex Dieter <rdieter@fedoraproject.org> - 5.12.0-1
- 5.12.0

* Wed Jun 17 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 5.11.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Wed Jun 10 2015 Daniel Vrátil <dvratil@redhat.com> - 5.11.0-1
- KDE Frameworks 6.11.0

* Mon May 11 2015 Daniel Vrátil <dvratil@redhat.com> - 5.10.0-1
- KDE Frameworks 6.10.0

* Sat May 02 2015 Kalev Lember <kalevlember@gmail.com> - 5.9.0-2
- Rebuilt for GCC 5 C++11 ABI change

* Tue Apr 07 2015 Daniel Vrátil <dvratil@redhat.com> - 5.9.0-1
- KDE Frameworks 6.9.0

* Mon Mar 16 2015 Daniel Vrátil <dvratil@redhat.com> - 5.8.0-1
- KDE Frameworks 6.8.0

* Fri Feb 27 2015 Daniel Vrátil <dvratil@redhat.com> - 5.7.0-2
- Rebuild (GCC 5)

* Mon Feb 16 2015 Daniel Vrátil <dvratil@redhat.com> - 5.7.0-1
- KDE Frameworks 6.7.0

* Thu Jan 08 2015 Daniel Vrátil <dvratil@redhat.com> - 5.6.0-1
- KDE Frameworks 6.6.0

* Mon Dec 08 2014 Daniel Vrátil <dvratil@redhat.com> - 5.5.0-1
- KDE Frameworks 6.5.0

* Mon Nov 03 2014 Daniel Vrátil <dvratil@redhat.com> - 5.4.0-1
- KDE Frameworks 6.4.0

* Tue Oct 07 2014 Daniel Vrátil <dvratil@redhat.com> - 5.3.0-1
- KDE Frameworks 6.3.0

* Thu Sep 11 2014 Daniel Vrátil <dvratil@redhat.com> - 5.2.0-1
- KDE Frameworks 6.2.0

* Sat Aug 16 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 5.1.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Wed Aug 06 2014 Daniel Vrátil <dvratil@redhat.com> - 5.1.0-1
- KDE Frameworks 6.1.0

* Wed Jul 09 2014 Daniel Vrátil <dvratil@redhat.com> - 5.0.0-1
- KDE Frameworks 6.0.0

* Sun Jun 08 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.100.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Tue Jun 03 2014 Daniel Vrátil <dvratil@redhat.com> - 4.100.0-1
- KDE Frameworks 4.100.0

* Tue May 06 2014 Daniel Vrátil <dvratil@redhat.com> - 4.99.0-2
- Rebuild against updated kf6-rpm-macros

* Mon May 05 2014 Daniel Vrátil <dvratil@redhat.com> - 4.99.0-1
- KDE Frameworks 4.99.0

* Mon Mar 31 2014 Jan Grulich <jgrulich@redhat.com> 4.98.0-1
- Update to KDE Frameworks 6 Beta 1 (4.98.0)

* Wed Mar 05 2014 Jan Grulich <jgrulich@redhat.com> 4.97.0-1
- Update to KDE Frameworks 6 Alpha 1 (4.97.0)

* Wed Feb 12 2014 Daniel Vrátil <dvratil@redhat.com> 4.96.0-1
- Update to KDE Frameworks 6 Alpha 1 (4.96.0)

* Wed Feb 05 2014 Daniel Vrátil <dvratil@redhat.com> 4.96.0-0.1.20140205git
- Update to pre-release snapshot of 4.96.0

* Thu Jan 09 2014 Daniel Vrátil <dvratil@redhat.com> 4.95.0-1
- Update to KDE Frameworks 6 TP1 (4.95.0)

* Sat Jan  4 2014 Daniel Vrátil <dvratil@redhat.com>
- initial version
