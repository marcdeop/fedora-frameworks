%global gitdate 20230829.233400
%global cmakever 5.240.0
%global commit0 5d65d17cca892d42f3ff2193ad16066dbdca32bb
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})


%global framework kstatusnotifieritem

Name:           kf6-%{framework}
Version:        %{cmakever}^%{gitdate}.%{shortcommit0}
Release:        48%{?dist}
Summary:        Implementation of Status Notifier Items

License:        LGPL-2.0-or-later
URL:            https://invent.kde.org/frameworks/kstatusnotifieritem
Source0:        %{framework}-%{shortcommit0}.tar.gz

BuildRequires:  extra-cmake-modules
BuildRequires:  cmake(Qt6Widgets)
BuildRequires:  cmake(Qt6DBus)
BuildRequires:  cmake(KF6WindowSystem)
BuildRequires:  pkgconfig(x11)

%description
%summary.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -n %{framework}-%{shortcommit0}

%build
%cmake_kf6 -DBUILD_WITH_QT6=ON
%cmake_build

%install
%cmake_install

%files
/usr/lib64/libKF6StatusNotifierItem.so.*
/usr/lib64/qt6/mkspecs/modules/qt_KStatusNotifierItem.pri
/usr/share/dbus-1/interfaces/kf6_org.kde.StatusNotifierItem.xml
/usr/share/dbus-1/interfaces/kf6_org.kde.StatusNotifierWatcher.xml
/usr/share/locale/ca/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/locale/ca@valencia/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/locale/eo/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/locale/es/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/locale/eu/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/locale/fr/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/locale/gl/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/locale/ia/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/locale/ka/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/locale/ko/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/locale/nl/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/locale/nn/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/locale/sl/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/locale/tr/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/locale/uk/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/locale/zh_CN/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/qlogging-categories6/kstatusnotifieritem.categories

%files devel
/usr/include/KF6/KStatusNotifierItem
/usr/lib64/cmake/KF6StatusNotifierItem
/usr/lib64/libKF6StatusNotifierItem.so

%changelog
* Tue Sep 19 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-48
- rebuilt

* Mon Sep 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-47
- rebuilt

* Mon Sep 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-46
- rebuilt

* Mon Sep 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-45
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-44
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-43
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-42
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-41
- rebuilt

* Thu Sep 14 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-40
- rebuilt

* Tue Sep 12 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-39
- rebuilt

* Tue Sep 12 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-38
- rebuilt

* Mon Sep 11 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-37
- rebuilt

* Sun Sep 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-36
- rebuilt

* Sun Sep 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-35
- rebuilt

* Sat Sep 09 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-34
- rebuilt

* Sat Sep 09 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-33
- rebuilt

* Thu Sep 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-32
- rebuilt

* Thu Sep 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-31
- rebuilt

* Thu Sep 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-30
- rebuilt

* Wed Sep 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-29
- rebuilt

* Wed Sep 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-28
- rebuilt

* Tue Sep 05 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-27
- rebuilt

* Mon Sep 04 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-26
- rebuilt

* Sun Sep 03 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-25
- rebuilt

* Sat Sep 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-24
- rebuilt

* Fri Sep 01 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-23
- rebuilt

* Thu Aug 31 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-22
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-21
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-20
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-19
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-18
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233400.5d65d17-17
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230821.012119.9a1a5c8-16
- rebuilt

* Tue Aug 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230821.012119.9a1a5c8-15
- rebuilt

* Mon Aug 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230821.012119.9a1a5c8-14
- rebuilt

* Sun Aug 27 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230821.012119.9a1a5c8-13
- rebuilt

* Sat Aug 26 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230821.012119.9a1a5c8-12
- rebuilt

* Sat Aug 26 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230821.012119.9a1a5c8-11
- rebuilt

* Fri Aug 25 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230821.012119.9a1a5c8-10
- rebuilt

* Thu Aug 24 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230821.012119.9a1a5c8-9
- rebuilt

* Wed Aug 23 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230821.012119.9a1a5c8-8
- rebuilt

* Tue Aug 22 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230821.012119.9a1a5c8-7
- rebuilt

* Mon Aug 21 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230818.235052.8082664-6
- rebuilt

* Mon Aug 21 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230818.235052.8082664-5
- rebuilt

* Sun Aug 20 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230817.133437.41011d8-4
- rebuilt

* Sat Aug 19 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230817.133437.41011d8-3
- rebuilt

* Fri Aug 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230816.133756.0d4a8ec-2
- rebuilt

* Thu Aug 17 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0-1
- Initial Package
