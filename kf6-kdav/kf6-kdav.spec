%global gitdate 20230829.232919
%global cmakever 5.240.0
%global commit0 c93f741fe60abe8431acd605966129112198adfd
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global date 20221109
%global framework kdav

# uncomment to enable bootstrap mode
%global bootstrap 1

%if !0%{?bootstrap}
%global tests 1
%endif

Name:    kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 130%{?dist}
Summary: A DAV protocol implementation with KJobs

License: GPLv2+
URL:     https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)

Source0:  https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

BuildRequires:  extra-cmake-modules
BuildRequires:  kf6-rpm-macros

BuildRequires:  cmake(Qt6Gui)
#BuildRequires:  cmake(Qt6XmlPatterns)

BuildRequires:  cmake(KF6I18n)
BuildRequires:  cmake(KF6CoreAddons)
BuildRequires:  cmake(KF6KIO) 

BuildRequires:  cmake(KF6CoreAddons)
BuildRequires:  cmake(KF6I18n)
BuildRequires:  cmake(KF6KIO)

%if 0%{?tests}
BuildRequires: cmake(Qt6Test)
BuildRequires: dbus-x11
BuildRequires: xorg-x11-server-Xvfb
%endif

%description
%{summary}.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       cmake(KF6CoreAddons)
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{shortcommit0} -p1


%build
%cmake_kf6 \
  -DBUILD_TESTING:BOOL=%{?tests:ON}%{!?tests:OFF}

%cmake_build


%install
%cmake_install

%find_lang %{name} --all-name --with-html


%check
%if 0%{?tests}
export CTEST_OUTPUT_ON_FAILURE=1
xvfb-run -a \
dbus-launch --exit-with-session \
make test ARGS="--output-on-failure --timeout 300" -C %{_target_platform} ||:
%endif


%ldconfig_scriptlets

%files -f %{name}.lang
%doc README*
%license LICENSES/*.txt
%{_kf6_datadir}/qlogging-categories6/*%{framework}.*
%{_kf6_libdir}/libKF6DAV.so.6
%{_kf6_libdir}/libKF6DAV.so.5.240.0

%files devel
%{_kf6_includedir}/KDAV/
%{_kf6_libdir}/libKF6DAV.so
%{_kf6_libdir}/cmake/KF6DAV/
%{_kf6_archdatadir}/mkspecs/modules/qt_KDAV.pri


%changelog
* Tue Sep 19 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-130
- rebuilt

* Mon Sep 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-129
- rebuilt

* Mon Sep 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-128
- rebuilt

* Mon Sep 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-127
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-126
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-125
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-124
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-123
- rebuilt

* Thu Sep 14 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-122
- rebuilt

* Tue Sep 12 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-121
- rebuilt

* Tue Sep 12 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-120
- rebuilt

* Mon Sep 11 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-119
- rebuilt

* Sun Sep 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-118
- rebuilt

* Sun Sep 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-117
- rebuilt

* Sat Sep 09 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-116
- rebuilt

* Sat Sep 09 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-115
- rebuilt

* Thu Sep 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-114
- rebuilt

* Thu Sep 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-113
- rebuilt

* Thu Sep 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-112
- rebuilt

* Wed Sep 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-111
- rebuilt

* Wed Sep 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-110
- rebuilt

* Tue Sep 05 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-109
- rebuilt

* Mon Sep 04 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-108
- rebuilt

* Sun Sep 03 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-107
- rebuilt

* Sat Sep 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-106
- rebuilt

* Fri Sep 01 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-105
- rebuilt

* Thu Aug 31 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-104
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-103
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-102
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-101
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-100
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232919.c93f741-99
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230828.123257.173b789-98
- rebuilt

* Tue Aug 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.132053.b57d513-97
- rebuilt

* Mon Aug 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.132053.b57d513-96
- rebuilt

* Sun Aug 27 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.132053.b57d513-95
- rebuilt

* Sat Aug 26 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.132053.b57d513-94
- rebuilt

* Sat Aug 26 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.132053.b57d513-93
- rebuilt

* Fri Aug 25 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.132053.b57d513-92
- rebuilt

* Thu Aug 24 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.132053.b57d513-91
- rebuilt

* Wed Aug 23 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.132053.b57d513-90
- rebuilt

* Tue Aug 22 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.132053.b57d513-89
- rebuilt

* Mon Aug 21 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.132053.b57d513-88
- rebuilt

* Mon Aug 21 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.132053.b57d513-87
- rebuilt

* Sun Aug 20 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.132053.b57d513-86
- rebuilt

* Sat Aug 19 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.132053.b57d513-85
- rebuilt

* Fri Aug 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.132053.b57d513-84
- rebuilt

* Thu Aug 17 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.132053.b57d513-83
- rebuilt

* Wed Aug 16 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.132053.b57d513-82
- rebuilt

* Tue Aug 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.132053.b57d513-81
- rebuilt

* Tue Aug 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.132053.b57d513-80
- rebuilt

* Tue Aug 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.132053.b57d513-79
- rebuilt

* Sun Aug 13 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.132053.b57d513-78
- rebuilt

* Sun Aug 13 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.132053.b57d513-77
- rebuilt

* Fri Aug 11 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.072438.b57d513-76
- rebuilt

* Thu Aug 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.072438.b57d513-75
- rebuilt

* Thu Aug 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.072438.b57d513-74
- rebuilt

* Thu Aug 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.072438.b57d513-73
- rebuilt

* Wed Aug 09 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.072438.b57d513-72
- rebuilt

* Tue Aug 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.072438.b57d513-71
- rebuilt

* Mon Aug 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.072438.b57d513-70
- rebuilt

* Sun Aug 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.072438.b57d513-69
- rebuilt

* Sun Aug 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.072438.b57d513-68
- rebuilt

* Sun Aug 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.072438.b57d513-67
- rebuilt

* Sun Aug 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.072438.b57d513-66
- rebuilt

* Sat Aug 05 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.072438.b57d513-65
- rebuilt

* Fri Aug 04 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.072438.b57d513-64
- rebuilt

* Fri Aug 04 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.072438.b57d513-63
- rebuilt

* Thu Aug 03 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.072438.b57d513-62
- rebuilt

* Wed Aug 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.072438.b57d513-61
- rebuilt

* Wed Aug 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.072438.b57d513-60
- rebuilt

* Tue Aug 01 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.072438.b57d513-59
- rebuilt

* Mon Jul 31 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.072438.b57d513-58
- rebuilt

* Mon Jul 31 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.072438.b57d513-57
- rebuilt

* Mon Jul 31 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.072438.b57d513-56
- rebuilt

* Sun Jul 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.072438.b57d513-55
- rebuilt

* Sun Jul 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.072438.b57d513-54
- rebuilt

* Sat Jul 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.072438.b57d513-53
- rebuilt

* Fri Jul 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230727.072438.b57d513-52
- rebuilt

* Fri Jul 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230725.131334.9a25ac5-51
- rebuilt

* Thu Jul 27 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230725.131334.9a25ac5-50
- rebuilt

* Wed Jul 26 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.214438.127272f-49
- rebuilt

* Tue Jul 25 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.214438.127272f-48
- rebuilt

* Mon Jul 24 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.214438.127272f-47
- rebuilt

* Mon Jul 24 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.214438.127272f-46
- rebuilt

* Sat Jul 22 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.214438.127272f-45
- rebuilt

* Fri Jul 21 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.214438.127272f-44
- rebuilt

* Thu Jul 20 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.214438.127272f-43
- rebuilt

* Tue Jul 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.214438.127272f-42
- rebuilt

* Tue Jul 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.214438.127272f-41
- rebuilt

* Tue Jul 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.214438.127272f-40
- rebuilt

* Mon Jul 17 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.214438.127272f-39
- rebuilt

* Sun Jul 16 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230629.214438.127272f-38
- rebuilt

* Sun Jul 16 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230629.214438.127272f-37
- rebuilt

* Sat Jul 15 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230629.214438.127272f-36
- rebuilt

* Sat Jul 15 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230629.214438.127272f-35
- rebuilt

* Fri Jul 14 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230629.214438.127272f-34
- rebuilt

* Fri Jul 14 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230629.214438.127272f-33
- rebuilt

* Fri Jul 14 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230629.214438.127272f-32
- rebuilt

* Fri Jul 14 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230629.214438.127272f-31
- rebuilt

* Thu Jul 13 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230629.214438.127272f-30
- rebuilt

* Thu Jul 13 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230629.214438.127272f-29
- rebuilt

* Thu Jul 13 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230629.214438.127272f-28
- rebuilt

* Mon Jul 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.214438.127272f-27
- rebuilt

* Sun Jul 09 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230629.214438.127272f-26
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230629.214438.127272f-25
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.214438.127272f-24
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.214438.127272f-23
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.214438.127272f-22
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.214438.127272f-21
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.214438.127272f-20
- rebuilt

* Thu Jul 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.214438.127272f-19
- rebuilt

* Wed Jul 05 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.214438.127272f-18
- rebuilt

* Tue Jul 04 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.214438.127272f-17
- rebuilt

* Sun Jul 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230629.214438.127272f-16
- rebuilt

* Sun Jul 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230404.031618.e989e9c-15
- rebuilt

* Sat Jul 01 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230404.031618.e989e9c-14
- rebuilt

* Thu Jun 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230404.031618.e989e9c-13
- rebuilt

* Wed Jun 28 2023 Justin - 5.240.0^20230404.031618.e989e9c-12
- rebuilt

* Tue Jun 27 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230404.031618.e989e9c-11
- rebuilt

* Sun Jun 25 2023 Justin - 5.240.0^20230404.031618.e989e9c-10
- rebuilt

* Sun Jun 25 2023 Justin - 5.240.0^20230404.031618.e989e9c-9
- rebuilt

* Sun Jun 25 2023 Justin - 5.240.0^20230404.031618.e989e9c-8
- rebuilt

* Fri Jun 23 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230530.030004.e989e9c-7
- rebuilt

* Fri Jun 02 2023 justin - 5.240.0^20230530.030004.e989e9c-6
- rebuilt

* Fri Jun 02 2023 justin - 5.240.0^20230530.030004.f9c1c4a-5
- rebuilt

* Fri Jun 02 2023 justin - 5.240.0^20230530.030004.f9c1c4a-4
- rebuilt

* Wed May 31 2023 justin - 5.240.0^20230530.030004.f9c1c4a-3
- rebuilt

* Wed May 31 2023 justin - 5.240.0^20230530.030004.f9c1c4a-2
- rebuilt

* Sat Aug 13 2022 Justin Zobel <justin@1707.io> - 5.97.0-1
- Update to 5.97.0

* Thu Jul 21 2022 Fedora Release Engineering <releng@fedoraproject.org> - 1:5.96.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_37_Mass_Rebuild

* Sun Jul 03 2022 Marc Deop <marcdeop@fedoraproject.org> - 1:5.96.0-1
- 5.96.0

* Fri May 13 2022 Rex Dieter <rdieter@fedoraproject.org> - 1:5.94.0-1
- 5.94.0

* Sun Apr 10 2022 Justin Zobel <justin@1707.io> - 5.93-1
- Update to 5.93

* Thu Mar 10 2022 Rex Dieter <rdieter@fedoraproject.org> - 1:5.92.0-1
- 5.92.0

* Fri Feb 11 2022 Rex Dieter <rdieter@fedoraproject.org> - 1:5.91.0-1
- 5.91.0

* Thu Jan 20 2022 Fedora Release Engineering <releng@fedoraproject.org> - 1:5.90.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_36_Mass_Rebuild

* Tue Jan 04 2022 Marc Deop i Argemí (Private) <marc@marcdeop.com> - 1:5.90.0-1
- 5.90.0

* Wed Dec 08 2021 Rex Dieter <rdieter@fedoraproject.org> - 1:5.89.0-1
- 5.89.0

* Mon Nov 08 2021 Marc Deop <marcdeop@fedoraproject.org> - 1:5.88.0-1
- 5.88.0

* Tue Oct 05 2021 Rex Dieter <rdieter@fedoraproject.org> - 1:5.87.0-1
- 5.87.0

* Tue Sep 14 2021 Marc Deop <marcdeop@fedoraproject.org> - 1:5.86.0-1
- 5.86.0

* Thu Aug 12 2021 Rex Dieter <rdieter@fedoraproject.org> - 1:5.85.0-1
- 5.85.0

* Thu Jul 22 2021 Fedora Release Engineering <releng@fedoraproject.org> - 1:5.83.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_35_Mass_Rebuild

* Tue Jun 08 2021 Rex Dieter <rdieter@fedoraproject.org> - 1:5.83.0-1
- 5.83.0

* Mon May 03 2021 Rex Dieter <rdieter@fedoraproject.org> - 1:5.82.0-1
- 5.82.0

* Tue Apr 06 2021 Rex Dieter <rdieter@fedoraproject.org> - 1:5.81.0-1
- 5.81.0

* Tue Mar 09 2021 Rex Dieter <rdieter@fedoraproject.org> - 1:5.80.0-1
- 5.80.0

* Sat Feb 06 2021 Rex Dieter <rdieter@fedoraproject.org> - 1:5.79.0-2
- respin

* Sat Feb 06 2021 Rex Dieter <rdieter@fedoraproject.org> - 1:5.79.0-1
- 5.79.0

* Tue Jan 26 2021 Fedora Release Engineering <releng@fedoraproject.org> - 1:5.78.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_34_Mass_Rebuild

* Mon Jan  4 08:43:16 CST 2021 Rex Dieter <rdieter@fedoraproject.org> - 1:5.78.0-1
- 5.78.0

* Sun Dec 13 14:10:36 CST 2020 Rex Dieter <rdieter@fedoraproject.org> - 1:5.77.0-1
- 5.77.0

* Thu Nov 19 08:58:46 CST 2020 Rex Dieter <rdieter@fedoraproject.org> - 1:5.76.0-1
- 5.76.0

* Wed Oct 14 09:50:02 CDT 2020 Rex Dieter <rdieter@fedoraproject.org> - 1:5.75.0-1
- 5.75.0

* Fri Sep 18 2020 Jan Grulich <jgrulich@redhat.com> - 1:5.74.0-1
- 5.74.0

* Mon Aug 03 2020 Rex Dieter <rdieter@fedoraproject.org> - 1:5.73.0-1
- 5.73.0

* Tue Jul 28 2020 Fedora Release Engineering <releng@fedoraproject.org> - 1:5.72.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_33_Mass_Rebuild

* Fri Jul 17 2020 Rex Dieter <rdieter@fedoraproject.org> - 1:5.72-1
- moved to kde frameworks, epoch++ for upgrade path

* Fri Jul 10 2020 Rex Dieter <rdieter@fedoraproject.org> - 20.04.3-1
- 20.04.3

* Fri Jun 12 2020 Rex Dieter <rdieter@fedoraproject.org> - 20.04.2-1
- 20.04.2

* Wed May 27 2020 Rex Dieter <rdieter@fedoraproject.org> - 20.04.1-1
- 20.04.1

* Fri Apr 24 2020 Rex Dieter <rdieter@fedoraproject.org> - 20.04.0-1
- 20.04.0

* Sat Mar 07 2020 Rex Dieter <rdieter@fedoraproject.org> - 19.12.3-1
- 19.12.3

* Tue Feb 04 2020 Rex Dieter <rdieter@fedoraproject.org> - 19.12.2-1
- 19.12.2

* Wed Jan 29 2020 Fedora Release Engineering <releng@fedoraproject.org> - 19.12.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_32_Mass_Rebuild

* Sat Jan 18 2020 Rex Dieter <rdieter@fedoraproject.org> - 19.12.1-1
- 19.12.1

* Mon Nov 11 2019 Rex Dieter <rdieter@fedoraproject.org> - 19.08.3-1
- 19.08.3

* Fri Oct 18 2019 Rex Dieter <rdieter@fedoraproject.org> - 19.08.2-1
- 19.08.2

* Thu Jul 25 2019 Fedora Release Engineering <releng@fedoraproject.org> - 19.04.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_31_Mass_Rebuild

* Fri Jul 12 2019 Rex Dieter <rdieter@fedoraproject.org> - 19.04.3-1
- 19.04.3

* Wed Jun 05 2019 Rex Dieter <rdieter@fedoraproject.org> - 19.04.2-1
- 19.04.2

* Fri Mar 08 2019 Rex Dieter <rdieter@fedoraproject.org> - 18.12.3-1
- 18.12.3

* Tue Feb 05 2019 Rex Dieter <rdieter@fedoraproject.org> - 18.12.2-1
- 18.12.2

* Fri Feb 01 2019 Fedora Release Engineering <releng@fedoraproject.org> - 18.12.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_30_Mass_Rebuild

* Tue Jan 08 2019 Rex Dieter <rdieter@fedoraproject.org> - 18.12.1-1
- 18.12.1

* Fri Dec 14 2018 Rex Dieter <rdieter@fedoraproject.org> - 18.12.0-1
- 18.12.0

* Tue Nov 06 2018 Rex Dieter <rdieter@fedoraproject.org> - 18.08.3-1
- 18.08.3

* Wed Oct 10 2018 Rex Dieter <rdieter@fedoraproject.org> - 18.08.2-1
- 18.08.2

* Mon Oct 01 2018 Rex Dieter <rdieter@fedoraproject.org> - 18.08.1-1
- 18.08.1

* Fri Jul 13 2018 Rex Dieter <rdieter@fedoraproject.org> - 18.04.3-1
- 18.04.3

* Fri Jul 13 2018 Fedora Release Engineering <releng@fedoraproject.org> - 18.04.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Wed Jun 06 2018 Rex Dieter <rdieter@fedoraproject.org> - 18.04.2-1
- 18.04.2

* Wed May 09 2018 Rex Dieter <rdieter@fedoraproject.org> - 18.04.1-1
- 18.04.1

* Fri Apr 20 2018 Rex Dieter <rdieter@fedoraproject.org> - 18.04.0-1
- 18.04.0

* Tue Mar 06 2018 Rex Dieter <rdieter@fedoraproject.org> - 17.12.3-1
- 17.12.3

* Tue Feb 06 2018 Rex Dieter <rdieter@fedoraproject.org> - 17.12.2-1
- 17.12.2

* Thu Jan 11 2018 Rex Dieter <rdieter@fedoraproject.org> - 17.12.1-1
- 17.12.1

* Tue Dec 12 2017 Rex Dieter <rdieter@fedoraproject.org> - 17.12.0-1
- 17.12.0

* Wed Dec 06 2017 Rex Dieter <rdieter@fedoraproject.org> - 17.11.90-1
- 17.11.90

* Wed Nov 22 2017 Rex Dieter <rdieter@fedoraproject.org> - 17.11.80-1
- 17.11.80

* Wed Nov 08 2017 Rex Dieter <rdieter@fedoraproject.org> - 17.08.3-1
- 17.08.3

* Mon Sep 25 2017 Rex Dieter <rdieter@fedoraproject.org> - 17.08.1-1
- 17.08.1

* Fri Jul 28 2017 Rex Dieter <rdieter@fedoraproject.org> - 17.04.3-1
- 17.04.3

* Wed Jul 26 2017 Fedora Release Engineering <releng@fedoraproject.org> - 17.04.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Thu Jun 15 2017 Rex Dieter <rdieter@fedoraproject.org> - 17.04.2-1
- 17.04.2

* Sun May 14 2017 Rex Dieter <rdieter@fedoraproject.org> - 17.04.1-2
- %%doc README, License: GPLv2+

* Thu May 11 2017 Rex Dieter <rdieter@fedoraproject.org> - 17.04.1-1
- 17.04.1, first try

