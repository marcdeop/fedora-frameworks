%global gitdate 20230828.194350
%global cmakever 5.240.0
%global commit0 5256bdeea62b4b4118e1fd2124c094c26c729a64
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global date 20221108


Name:    kuserfeedback
Summary: Framework for collecting user feedback for apps via telemetry and surveys
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 122%{?dist}

License: MIT
URL:     https://invent.kde.org/libraries/%{name}
Source0: %{name}-%{shortcommit0}.tar.gz

## upstream patches

BuildRequires: cmake
BuildRequires: gcc-c++

BuildRequires: kf6-rpm-macros
BuildRequires: libappstream-glib
BuildRequires: desktop-file-utils
BuildRequires: extra-cmake-modules

BuildRequires: cmake(Qt6Charts)
BuildRequires: cmake(Qt6Core)
BuildRequires: cmake(Qt6Network)
BuildRequires: cmake(Qt6PrintSupport)
BuildRequires: cmake(Qt6Qml)
BuildRequires: cmake(Qt6Svg)
BuildRequires: cmake(Qt6Test)
BuildRequires: cmake(Qt6Widgets)

BuildRequires: bison
BuildRequires: flex

%description
%{summary}.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       cmake(Qt6Network)
Requires:       cmake(Qt6Widgets)

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package        console
Summary:        Analytics and administration tool for UserFeedback servers
Requires:       %{name} = %{version}-%{release}
BuildRequires: qt6-qtbase-private-devel
Requires: qt6-qtcharts

%description    console
Analytics and administration tool for UserFeedback servers.


%prep
%autosetup -p1 -n %{name}-%{shortcommit0}


%build
%cmake_kf6 \
   -DENABLE_DOCS:BOOL=OFF \
   -DBUILD_WITH_QT6=ON -DQT_MAJOR_VERSION=6 

%cmake_build


%install
%cmake_install

%find_lang userfeedbackconsole5 --with-qt
%find_lang userfeedbackprovider5 --with-qt


%check
#appstream-util validate-relax --nonet %%{buildroot}%%{_kf6_metainfodir}/org.kde.kuserfeedback-console.appdata.xml
desktop-file-validate %{buildroot}%{_datadir}/applications/org.kde.kuserfeedback-console.desktop


%files -f userfeedbackprovider5.lang
%doc README.md
%license COPYING.LIB
%{_bindir}/userfeedbackctl
%{_libdir}/libKUserFeedbackCoreQt6.so.1*
%{_libdir}/libKUserFeedbackWidgetsQt6.so.1*
%{_kf6_qmldir}/org/kde/userfeedback/
%{_kf6_datadir}/metainfo/org.kde.kuserfeedback-console.appdata.xml
%{_kf6_datadir}/qlogging-categories6/org_kde_UserFeedback.categories


%files devel
%{_includedir}/KUserFeedbackQt6
%{_libdir}/libKUserFeedbackCoreQt6.so
%{_libdir}/libKUserFeedbackWidgetsQt6.so
%{_kf6_libdir}/cmake/KUserFeedbackQt6
%{_kf6_archdatadir}/mkspecs/modules/qt_KUserFeedback*.pri


%files console -f userfeedbackconsole5.lang
%{_bindir}/UserFeedbackConsole
%{_datadir}/applications/org.kde.kuserfeedback-console.desktop
#%%{_kf6_metainfodir}/org.kde.kuserfeedback-console.appdata.xml


%changelog
* Tue Sep 19 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230828.194350.5256bde-122
- rebuilt

* Mon Sep 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230828.194350.5256bde-121
- rebuilt

* Mon Sep 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230828.194350.5256bde-120
- rebuilt

* Mon Sep 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230828.194350.5256bde-119
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230828.194350.5256bde-118
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230828.194350.5256bde-117
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230828.194350.5256bde-116
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230828.194350.5256bde-115
- rebuilt

* Thu Sep 14 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230828.194350.5256bde-114
- rebuilt

* Tue Sep 12 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230828.194350.5256bde-113
- rebuilt

* Tue Sep 12 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230828.194350.5256bde-112
- rebuilt

* Mon Sep 11 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230828.194350.5256bde-111
- rebuilt

* Sun Sep 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230828.194350.5256bde-110
- rebuilt

* Sun Sep 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230828.194350.5256bde-109
- rebuilt

* Sat Sep 09 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230828.194350.5256bde-108
- rebuilt

* Sat Sep 09 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230828.194350.5256bde-107
- rebuilt

* Thu Sep 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230828.194350.5256bde-106
- rebuilt

* Thu Sep 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230828.194350.5256bde-105
- rebuilt

* Thu Sep 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230828.194350.5256bde-104
- rebuilt

* Wed Sep 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230828.194350.5256bde-103
- rebuilt

* Wed Sep 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230828.194350.5256bde-102
- rebuilt

* Tue Sep 05 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230828.194350.5256bde-101
- rebuilt

* Mon Sep 04 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230828.194350.5256bde-100
- rebuilt

* Sun Sep 03 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230828.194350.5256bde-99
- rebuilt

* Sat Sep 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230828.194350.5256bde-98
- rebuilt

* Fri Sep 01 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230828.194350.5256bde-97
- rebuilt

* Thu Aug 31 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230828.194350.5256bde-96
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230828.194350.5256bde-95
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230828.194350.5256bde-94
- rebuilt

* Tue Aug 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230820.153343.3ca0615-93
- rebuilt

* Mon Aug 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230820.153343.3ca0615-92
- rebuilt

* Sun Aug 27 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230820.153343.3ca0615-91
- rebuilt

* Sat Aug 26 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230820.153343.3ca0615-90
- rebuilt

* Sat Aug 26 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230820.153343.3ca0615-89
- rebuilt

* Fri Aug 25 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230820.153343.3ca0615-88
- rebuilt

* Thu Aug 24 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230820.153343.3ca0615-87
- rebuilt

* Wed Aug 23 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230820.153343.3ca0615-86
- rebuilt

* Tue Aug 22 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230820.153343.3ca0615-85
- rebuilt

* Mon Aug 21 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230820.153343.3ca0615-84
- rebuilt

* Mon Aug 21 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230816.183224.681a56c-83
- rebuilt

* Sun Aug 20 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230816.183224.681a56c-82
- rebuilt

* Sat Aug 19 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230816.183224.681a56c-81
- rebuilt

* Fri Aug 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230816.183224.681a56c-80
- rebuilt

* Thu Aug 17 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230804.152212.39d87ed-79
- rebuilt

* Wed Aug 16 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230804.152212.39d87ed-78
- rebuilt

* Tue Aug 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230804.152212.39d87ed-77
- rebuilt

* Tue Aug 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230804.152212.39d87ed-76
- rebuilt

* Tue Aug 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230804.152212.39d87ed-75
- rebuilt

* Sun Aug 13 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230804.152212.39d87ed-74
- rebuilt

* Sun Aug 13 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230804.152212.39d87ed-73
- rebuilt

* Fri Aug 11 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230715.224539.39d87ed-72
- rebuilt

* Thu Aug 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230715.224539.39d87ed-71
- rebuilt

* Thu Aug 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230715.224539.39d87ed-70
- rebuilt

* Wed Aug 09 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230715.224539.39d87ed-69
- rebuilt

* Tue Aug 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230715.224539.39d87ed-68
- rebuilt

* Mon Aug 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230715.224539.39d87ed-67
- rebuilt

* Sun Aug 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230715.224539.39d87ed-66
- rebuilt

* Sun Aug 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230715.224539.39d87ed-65
- rebuilt

* Sun Aug 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230720.074007.b668b38-64
- rebuilt

* Sat Aug 05 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230720.074007.b668b38-63
- rebuilt

* Fri Aug 04 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230720.074007.b668b38-62
- rebuilt

* Fri Aug 04 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230720.074007.b668b38-61
- rebuilt

* Thu Aug 03 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230720.074007.b668b38-60
- rebuilt

* Wed Aug 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230720.074007.b668b38-59
- rebuilt

* Wed Aug 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230720.074007.b668b38-58
- rebuilt

* Tue Aug 01 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230720.074007.b668b38-57
- rebuilt

* Mon Jul 31 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230720.074007.b668b38-56
- rebuilt

* Mon Jul 31 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230720.074007.b668b38-55
- rebuilt

* Mon Jul 31 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230720.074007.b668b38-54
- rebuilt

* Sun Jul 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230720.074007.b668b38-53
- rebuilt

* Sun Jul 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230720.074007.b668b38-52
- rebuilt

* Sat Jul 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230720.074007.b668b38-51
- rebuilt

* Fri Jul 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230720.074007.b668b38-50
- rebuilt

* Thu Jul 27 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230720.074007.b668b38-49
- rebuilt

* Wed Jul 26 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230720.074007.b668b38-48
- rebuilt

* Tue Jul 25 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230720.074007.b668b38-47
- rebuilt

* Mon Jul 24 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230720.074007.b668b38-46
- rebuilt

* Mon Jul 24 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230720.074007.b668b38-45
- rebuilt

* Sat Jul 22 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230720.074007.b668b38-44
- rebuilt

* Fri Jul 21 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230710.230207.89ca429-43
- rebuilt

* Thu Jul 20 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230710.230207.89ca429-42
- rebuilt

* Tue Jul 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230710.230207.89ca429-41
- rebuilt

* Tue Jul 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230710.230207.89ca429-40
- rebuilt

* Tue Jul 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230710.230207.89ca429-39
- rebuilt

* Mon Jul 17 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230710.230207.89ca429-38
- rebuilt

* Sun Jul 16 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230710.230207.89ca429-37
- rebuilt

* Sun Jul 16 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230710.230207.89ca429-36
- rebuilt

* Sat Jul 15 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230710.230207.89ca429-35
- rebuilt

* Sat Jul 15 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230710.230207.89ca429-34
- rebuilt

* Fri Jul 14 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230710.230207.89ca429-33
- rebuilt

* Fri Jul 14 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230710.230207.89ca429-32
- rebuilt

* Fri Jul 14 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230710.230207.89ca429-31
- rebuilt

* Fri Jul 14 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230710.230207.89ca429-30
- rebuilt

* Thu Jul 13 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230710.230207.89ca429-29
- rebuilt

* Thu Jul 13 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230710.230207.89ca429-28
- rebuilt

* Thu Jul 13 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230605.131422.d43b9ba-27
- rebuilt

* Mon Jul 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230605.131422.d43b9ba-26
- rebuilt

* Sun Jul 09 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230605.131422.d43b9ba-25
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230605.131422.d43b9ba-24
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230605.131422.d43b9ba-23
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230605.131422.d43b9ba-22
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230605.131422.d43b9ba-21
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230605.131422.d43b9ba-20
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230605.131422.d43b9ba-19
- rebuilt

* Thu Jul 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230605.131422.d43b9ba-18
- rebuilt

* Wed Jul 05 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230605.131422.d43b9ba-17
- rebuilt

* Tue Jul 04 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230605.131422.d43b9ba-16
- rebuilt

* Sun Jul 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230605.131422.d43b9ba-15
- rebuilt

* Sun Jul 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230605.131422.d43b9ba-14
- rebuilt

* Sat Jul 01 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230605.131422.d43b9ba-13
- rebuilt

* Thu Jun 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230605.131422.d43b9ba-12
- rebuilt

* Wed Jun 28 2023 Justin - 5.240.0^20230605.131422.d43b9ba-11
- rebuilt

* Tue Jun 27 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230605.131422.d43b9ba-10
- rebuilt

* Sun Jun 25 2023 Justin - 5.240.0^20230605.131422.d43b9ba-9
- rebuilt

* Sun Jun 25 2023 Justin - 5.240.0^20230605.131422.d43b9ba-8
- rebuilt

* Fri Jun 23 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230530.175226.f7efd97-7
- rebuilt

* Fri Jun 02 2023 justin - 5.240.0^20230531.021616.f1a17ec-6
- rebuilt

* Fri Jun 02 2023 justin - 5.240.0^20230531.021616.8193ac7-5
- rebuilt

* Fri Jun 02 2023 justin - 5.240.0^20230531.021616.8193ac7-4
- rebuilt

* Wed May 31 2023 justin - 5.240.0^20230531.021616.8193ac7-3
- rebuilt

* Wed May 31 2023 justin - 5.240.0^20230530.020304.a5bdcb3-2
- rebuilt

* Thu Jan 19 2023 Fedora Release Engineering <releng@fedoraproject.org> - 1.2.0-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_38_Mass_Rebuild

* Fri Sep 23 2022 Jan Grulich <jgrulich@redhat.com> - 1.2.0-7
- Bring back dependency on qt6-qtcharts

* Fri Sep 23 2022 Jan Grulich <jgrulich@redhat.com> - 1.2.0-6
- Drop hardcoded Qt version requirement

* Thu Jul 21 2022 Fedora Release Engineering <releng@fedoraproject.org> - 1.2.0-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_37_Mass_Rebuild

* Thu Jul 14 2022 Jan Grulich <jgrulich@redhat.com> - 1.2.0-4
- Rebuild (qt6)

* Tue May 17 2022 Jan Grulich <jgrulich@redhat.com> - 1.2.0-3
- Rebuild (qt6)

* Tue Mar 08 2022 Jan Grulich <jgrulich@redhat.com> - 1.2.0-2
- Rebuild (qt6)

* Fri Feb 04 2022 Yaroslav Sidlovsky <zawertun@gmail.com> - 1.2.0-1
- update to 1.2.0

* Fri Feb 04 2022 Rex Dieter <rdieter@fedoraproject.org> - 1.0.0-11
- -console: uses qt6-qtcharts private api
- -devel: use cmake-style deps instead of hard-coding qt6-qtbase

* Thu Feb 03 2022 Rex Dieter <rdieter@fedoraproject.org> - 1.0.0-10
- backport crash fix recommended by upstream
- cleanup macros
- simplify %%files
- BR: bison flex (enables Survey targeting expressions support)
- drop BR: qt6-qtbase-private-devel (no private api use detected)
- drop non-autodetected runtime deps

* Thu Jan 20 2022 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.0-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_36_Mass_Rebuild

* Thu Jul 22 2021 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.0-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_35_Mass_Rebuild

* Tue Jan 26 2021 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.0-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_34_Mass_Rebuild

* Tue Jan 12 21:50:41 MSK 2021 Yaroslav Sidlovsky <zawertun@gmail.com> - 1.0.0-6
- track Qt private api usage

* Tue Nov 24 13:19:14 MSK 2020 Yaroslav Sidlovsky <zawertun@gmail.com> - 1.0.0-5
- rebuild due to new Qt version

* Sun Sep 20 2020 Yaroslav Sidlovsky <zawertun@gmail.com> - 1.0.0-4
- one more rebuild

* Sat Sep 19 2020 Yaroslav Sidlovsky <zawertun@gmail.com> - 1.0.0-3
- rebuild

* Tue Jul 28 2020 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_33_Mass_Rebuild

* Sat Jun 06 2020 Yaroslav Sidlovsky <zawertun@gmail.com> - 1.0.0-1
- first spec for version 1.0.0

