%global gitdate 20230801.154407
%global cmakever 5.100.0
%global commit0 b6708c9551cf2af82cc68126403f7a3fa808c91e
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})

%global framework kapidox

Name:    kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 34%{?dist}
Summary: KDE Frameworks 6 Tier 4 scripts and data for building API documentation
License: BSD-3-Clause
URL:     https://invent.kde.org/frameworks/%{framework}
Source0: https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

BuildArch:      noarch

BuildRequires:  kf6-rpm-macros
BuildRequires:  python3
BuildRequires:  python3-charset-normalizer
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools

Requires:       kf6-filesystem
Requires:       python3-PyYAML
Requires:       python3-jinja2
Requires:       python3-requests

%description
Scripts and data for building API documentation (dox) in a standard format and
style.

%prep
%autosetup -n %{framework}-%{shortcommit0} -p1

%build
%py3_build

%install
%py3_install

%files
%license LICENSES/*.txt
%{_kf6_bindir}/depdiagram_generate_all
%{_kf6_bindir}/kapidox-depdiagram-generate
%{_kf6_bindir}/kapidox-depdiagram-prepare
%{_kf6_bindir}/kapidox-generate
%{python3_sitelib}/kapidox
%{python3_sitelib}/kapidox*-py*.egg-info

%changelog
* Tue Sep 19 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-34
- rebuilt

* Mon Sep 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-33
- rebuilt

* Mon Sep 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-32
- rebuilt

* Mon Sep 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-31
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-30
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-29
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-28
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-27
- rebuilt

* Thu Sep 14 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-26
- rebuilt

* Tue Sep 12 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-25
- rebuilt

* Tue Sep 12 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-24
- rebuilt

* Mon Sep 11 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-23
- rebuilt

* Sun Sep 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-22
- rebuilt

* Sun Sep 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-21
- rebuilt

* Sat Sep 09 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-20
- rebuilt

* Sat Sep 09 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-19
- rebuilt

* Thu Sep 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-18
- rebuilt

* Thu Sep 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-17
- rebuilt

* Thu Sep 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-16
- rebuilt

* Wed Sep 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-15
- rebuilt

* Wed Sep 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-14
- rebuilt

* Tue Sep 05 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-13
- rebuilt

* Mon Sep 04 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-12
- rebuilt

* Sun Sep 03 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-11
- rebuilt

* Sat Sep 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-10
- rebuilt

* Fri Sep 01 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-9
- rebuilt

* Thu Aug 31 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-8
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-7
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-6
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-5
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-4
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.100.0^20230801.154407.b6708c9-3
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154407.b6708c9-2
- rebuilt

* Tue Aug 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154407.b6708c9-101
- Initial Release
