%global gitdate 20230914.123515
%global cmakever 5.240.0
%global commit0 5203bed8bfb5c46ea3f2539d909fafc24a692ce3
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global date 20221109
%undefine __cmake_in_source_build
%global framework kservice

Name:    kf6-%{framework}
Summary: KDE Frameworks 6 Tier 3 solution for advanced plugin and service introspection
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 127%{?dist}

# mixture of LGPLv2 and LGPLv2+ (mostly the latter)
License: LGPLv2
URL:     https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)

Source0: https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

## upstream patches

## downstream patches
# Fedora customizations to the menu categories
# adds the Administration menu from redhat-menus which equals System + Settings
# This prevents the stuff getting listed twice, under both System and Settings.
Patch100:  kservice-5.15.0-xdg-menu.patch

# kbuildsycoca5 always gives:
# kf6.kservice.sycoca: Parse error in  "$HOME/.config/menus/applications-merged/xdg-desktop-menu-dummy.menu" , line  1 , col  1 :  "unexpected end of file"
# hide that by default, make it qCDebug instead (of qCWarning)
Patch101:  kservice-5.17.0-vfolder_spam.patch


BuildRequires:  extra-cmake-modules
BuildRequires:  cmake(KF6Config)
BuildRequires:  cmake(KF6CoreAddons)
BuildRequires:  kf6-kcrash-devel
BuildRequires:  cmake(KF6DBusAddons)
BuildRequires:  cmake(KF6DocTools)
BuildRequires:  cmake(KF6I18n)
BuildRequires:  kf6-rpm-macros
BuildRequires:  qt6-qtbase-devel

BuildRequires:  flex
BuildRequires:  bison

# for the Administration category
Recommends:       redhat-menus

%description
KDE Frameworks 6 Tier 3 solution for advanced plugin and service
introspection.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       cmake(KF6Config)
Requires:       cmake(KF6CoreAddons)
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{shortcommit0} -p1


%build
%{cmake_kf6}
%cmake_build


%install
%cmake_install

%find_lang %{name} --all-name --with-man

mv %{buildroot}%{_kf6_sysconfdir}/xdg/menus/applications.menu \
   %{buildroot}%{_kf6_sysconfdir}/xdg/menus/kf6-applications.menu

mkdir -p %{buildroot}%{_kf6_datadir}/kservices6
mkdir -p %{buildroot}%{_kf6_datadir}/kservicetypes6


%ldconfig_scriptlets

%files -f %{name}.lang
%doc README.md
%license LICENSES/*.txt
# this is not a config file, despite rpmlint complaining otherwise -- rex
%{_kf6_sysconfdir}/xdg/menus/kf6-applications.menu
%{_kf6_datadir}/qlogging-categories6/%{framework}.*
%{_kf6_bindir}/kbuildsycoca6
%{_kf6_libdir}/libKF6Service.so.6*
%{_kf6_libdir}/libKF6Service.so.5.240.0
%{_kf6_datadir}/kservicetypes6/
%{_kf6_datadir}/kservices6/
%{_kf6_mandir}/man8/*.8*

%files devel

%{_kf6_includedir}/KService/
%{_kf6_libdir}/libKF6Service.so
%{_kf6_libdir}/cmake/KF6Service/
%{_kf6_archdatadir}/mkspecs/modules/qt_KService.pri


%changelog
* Tue Sep 19 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230914.123515.5203bed-127
- rebuilt

* Mon Sep 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230914.123515.5203bed-126
- rebuilt

* Mon Sep 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230914.123515.5203bed-125
- rebuilt

* Mon Sep 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230914.123515.5203bed-124
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230914.123515.5203bed-123
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230914.123515.5203bed-122
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230914.123515.5203bed-121
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230906.214938.6d5956a-120
- rebuilt

* Thu Sep 14 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230906.214938.6d5956a-119
- rebuilt

* Tue Sep 12 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230906.214938.6d5956a-118
- rebuilt

* Tue Sep 12 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230906.214938.6d5956a-117
- rebuilt

* Mon Sep 11 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230906.214938.6d5956a-116
- rebuilt

* Sun Sep 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230906.214938.6d5956a-115
- rebuilt

* Sun Sep 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230906.214938.6d5956a-114
- rebuilt

* Sat Sep 09 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230906.214938.6d5956a-113
- rebuilt

* Sat Sep 09 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230906.214938.6d5956a-112
- rebuilt

* Thu Sep 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230902.131102.b32418a-111
- rebuilt

* Thu Sep 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230902.131102.b32418a-110
- rebuilt

* Thu Sep 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230902.131102.b32418a-109
- rebuilt

* Wed Sep 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230902.131102.b32418a-108
- rebuilt

* Wed Sep 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230902.131102.b32418a-107
- rebuilt

* Tue Sep 05 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230902.131102.b32418a-106
- rebuilt

* Mon Sep 04 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230902.131102.b32418a-105
- rebuilt

* Sun Sep 03 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233352.39bf3e9-104
- rebuilt

* Sat Sep 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233352.39bf3e9-103
- rebuilt

* Fri Sep 01 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233352.39bf3e9-102
- rebuilt

* Thu Aug 31 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233352.39bf3e9-101
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233352.39bf3e9-100
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233352.39bf3e9-99
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233352.39bf3e9-98
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233352.39bf3e9-97
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233352.39bf3e9-96
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-95
- rebuilt

* Tue Aug 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-94
- rebuilt

* Mon Aug 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-93
- rebuilt

* Sun Aug 27 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-92
- rebuilt

* Sat Aug 26 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-91
- rebuilt

* Sat Aug 26 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-90
- rebuilt

* Fri Aug 25 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-89
- rebuilt

* Thu Aug 24 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-88
- rebuilt

* Wed Aug 23 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-87
- rebuilt

* Tue Aug 22 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-86
- rebuilt

* Mon Aug 21 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-85
- rebuilt

* Mon Aug 21 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-84
- rebuilt

* Sun Aug 20 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-83
- rebuilt

* Sat Aug 19 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-82
- rebuilt

* Fri Aug 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-81
- rebuilt

* Thu Aug 17 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-80
- rebuilt

* Wed Aug 16 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-79
- rebuilt

* Tue Aug 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-78
- rebuilt

* Tue Aug 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-77
- rebuilt

* Tue Aug 15 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-76
- rebuilt

* Sun Aug 13 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-75
- rebuilt

* Sun Aug 13 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-74
- rebuilt

* Fri Aug 11 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-73
- rebuilt

* Thu Aug 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-72
- rebuilt

* Thu Aug 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-71
- rebuilt

* Wed Aug 09 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-70
- rebuilt

* Tue Aug 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-69
- rebuilt

* Mon Aug 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-68
- rebuilt

* Sun Aug 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-67
- rebuilt

* Sun Aug 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-66
- rebuilt

* Sun Aug 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-65
- rebuilt

* Sat Aug 05 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-64
- rebuilt

* Fri Aug 04 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-63
- rebuilt

* Fri Aug 04 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-62
- rebuilt

* Thu Aug 03 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-61
- rebuilt

* Wed Aug 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230801.154801.7e0929f-60
- rebuilt

* Wed Aug 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230716.114229.8874790-59
- rebuilt

* Tue Aug 01 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230716.114229.8874790-58
- rebuilt

* Mon Jul 31 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230716.114229.8874790-57
- rebuilt

* Mon Jul 31 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230716.114229.8874790-56
- rebuilt

* Mon Jul 31 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230716.114229.8874790-55
- rebuilt

* Sun Jul 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230716.114229.8874790-54
- rebuilt

* Sun Jul 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230716.114229.8874790-53
- rebuilt

* Sat Jul 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230708.172634.ed118c3-52
- rebuilt

* Fri Jul 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230708.172634.ed118c3-51
- rebuilt

* Fri Jul 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230708.172634.ed118c3-50
- rebuilt

* Thu Jul 27 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230708.172634.ed118c3-49
- rebuilt

* Wed Jul 26 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230708.172634.ed118c3-48
- rebuilt

* Tue Jul 25 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230708.172634.ed118c3-47
- rebuilt

* Mon Jul 24 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230708.172634.ed118c3-46
- rebuilt

* Mon Jul 24 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230708.172634.ed118c3-45
- rebuilt

* Sat Jul 22 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230708.172634.ed118c3-44
- rebuilt

* Fri Jul 21 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230708.172634.ed118c3-43
- rebuilt

* Thu Jul 20 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230708.172634.ed118c3-42
- rebuilt

* Tue Jul 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230708.172634.ed118c3-41
- rebuilt

* Tue Jul 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230708.172634.ed118c3-40
- rebuilt

* Tue Jul 18 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230708.172634.ed118c3-39
- rebuilt

* Mon Jul 17 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230708.172634.ed118c3-38
- rebuilt

* Sun Jul 16 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230708.172634.ed118c3-37
- rebuilt

* Sun Jul 16 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230708.172634.ed118c3-36
- rebuilt

* Sat Jul 15 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230708.172634.ed118c3-35
- rebuilt

* Sat Jul 15 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230708.172634.ed118c3-34
- rebuilt

* Fri Jul 14 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230708.172634.ed118c3-33
- rebuilt

* Fri Jul 14 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230708.172634.ed118c3-32
- rebuilt

* Fri Jul 14 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230708.172634.ed118c3-31
- rebuilt

* Fri Jul 14 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230708.172634.ed118c3-30
- rebuilt

* Thu Jul 13 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230708.172634.ed118c3-29
- rebuilt

* Thu Jul 13 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230708.172634.ed118c3-28
- rebuilt

* Thu Jul 13 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230708.172634.ed118c3-27
- rebuilt

* Mon Jul 10 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230708.172634.ed118c3-26
- rebuilt

* Sun Jul 09 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230704.173709.17f748b-25
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin@1707.io> - 5.240.0^20230704.173709.17f748b-24
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230704.173709.17f748b-23
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230704.173709.17f748b-22
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230704.173709.17f748b-21
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230704.173709.17f748b-20
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230704.173709.17f748b-19
- rebuilt

* Thu Jul 06 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230704.173709.17f748b-18
- rebuilt

* Wed Jul 05 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230703.004442.c05ebe9-17
- rebuilt

* Tue Jul 04 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.002530.ec4205f-16
- rebuilt

* Sun Jul 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230630.002530.ec4205f-15
- rebuilt

* Sun Jul 02 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230616.174519.a9d53fe-14
- rebuilt

* Sat Jul 01 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230616.174519.a9d53fe-13
- rebuilt

* Thu Jun 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230616.174519.a9d53fe-12
- rebuilt

* Wed Jun 28 2023 Justin - 5.240.0^20230616.174519.a9d53fe-11
- rebuilt

* Tue Jun 27 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230616.174519.a9d53fe-10
- rebuilt

* Sun Jun 25 2023 Justin - 5.240.0^20230616.174519.a9d53fe-9
- rebuilt

* Sun Jun 25 2023 Justin - 5.240.0^20230616.174519.a9d53fe-8
- rebuilt

* Fri Jun 23 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230601.023125.4f4b228-7
- rebuilt

* Fri Jun 02 2023 justin - 5.240.0^20230601.023125.4f4b228-6
- rebuilt

* Fri Jun 02 2023 justin - 5.240.0^20230601.023125.5f1b046-5
- rebuilt

* Fri Jun 02 2023 justin - 5.240.0^20230530.030330.67f8bb1-4
- rebuilt

* Wed May 31 2023 justin - 5.240.0^20230530.030330.67f8bb1-3
- rebuilt

* Wed May 31 2023 justin - 5.240.0^20230530.030330.67f8bb1-2
- rebuilt

* Sat Aug 13 2022 Justin Zobel <justin@1707.io> - 5.97.0-1
- Update to 5.97.0

* Thu Jul 21 2022 Fedora Release Engineering <releng@fedoraproject.org> - 5.96.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_37_Mass_Rebuild

* Sun Jul 03 2022 Marc Deop <marcdeop@fedoraproject.org> - 5.96.0-1
- 5.96.0
- remove patch as it is included upstream

* Tue May 17 2022 Timothée Ravier <tim@siosm.fr> - 5.94.0-2
- Fixes rhbz#2021087

* Fri May 13 2022 Rex Dieter <rdieter@fedoraproject.org> - 5.94.0-1
- 5.94.0

* Sun Apr 10 2022 Justin Zobel <justin@1707.io> - 5.93-1
- Update to 5.93

* Thu Mar 10 2022 Rex Dieter <rdieter@fedoraproject.org> - 5.92.0-1
- 5.92.0

* Fri Feb 11 2022 Rex Dieter <rdieter@fedoraproject.org> - 5.91.0-1
- 5.91.0

* Thu Jan 20 2022 Fedora Release Engineering <releng@fedoraproject.org> - 5.90.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_36_Mass_Rebuild

* Tue Jan 04 2022 Marc Deop i Argemí (Private) <marc@marcdeop.com> - 5.90.0-1
- 5.90.0

* Wed Dec 08 2021 Rex Dieter <rdieter@fedoraproject.org> - 5.89.0-1
- 5.89.0

* Mon Nov 08 2021 Marc Deop <marcdeop@fedoraproject.org> - 5.88.0-1
- 5.88.0

* Tue Oct 05 2021 Rex Dieter <rdieter@fedoraproject.org> - 5.87.0-1
- 5.87.0

* Tue Sep 14 2021 Marc Deop <marcdeop@fedoraproject.org> - 5.86.0-1
- 5.86.0

* Thu Aug 12 2021 Rex Dieter <rdieter@fedoraproject.org> - 5.85.0-1
- 5.85.0

* Thu Jul 22 2021 Fedora Release Engineering <releng@fedoraproject.org> - 5.83.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_35_Mass_Rebuild

* Tue Jun 08 2021 Rex Dieter <rdieter@fedoraproject.org> - 5.83.0-1
- 5.83.0

* Mon May 03 2021 Rex Dieter <rdieter@fedoraproject.org> - 5.82.0-1
- 5.82.0

* Tue Apr 06 2021 Rex Dieter <rdieter@fedoraproject.org> - 5.81.0-1
- 5.81.0

* Tue Mar 09 2021 Rex Dieter <rdieter@fedoraproject.org> - 5.80.0-1
- 5.80.0

* Sat Feb 06 2021 Rex Dieter <rdieter@fedoraproject.org> - 5.79.0-2
- respin

* Sat Feb 06 2021 Rex Dieter <rdieter@fedoraproject.org> - 5.79.0-1
- 5.79.0

* Tue Jan 26 2021 Fedora Release Engineering <releng@fedoraproject.org> - 5.78.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_34_Mass_Rebuild

* Mon Jan  4 08:54:38 CST 2021 Rex Dieter <rdieter@fedoraproject.org> - 5.78.0-1
- 5.78.0

* Sun Dec 13 14:19:03 CST 2020 Rex Dieter <rdieter@fedoraproject.org> - 5.77.0-1
- 5.77.0

* Thu Nov 19 09:11:20 CST 2020 Rex Dieter <rdieter@fedoraproject.org> - 5.76.0-1
- 5.76.0

* Wed Oct 14 10:04:29 CDT 2020 Rex Dieter <rdieter@fedoraproject.org> - 5.75.0-1
- 5.75.0

* Fri Sep 18 2020 Jan Grulich <jgrulich@redhat.com> - 5.74.0-1
- 5.74.0

* Mon Aug 03 2020 Rex Dieter <rdieter@fedoraproject.org> - 5.73.0-1
- 5.73.0

* Tue Jul 28 2020 Fedora Release Engineering <releng@fedoraproject.org> - 5.72.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_33_Mass_Rebuild

* Tue Jul 07 2020 Rex Dieter <rdieter@fedoraproject.org> - 5.72.0-1
- 5.72.0

* Tue Jun 16 2020 Rex Dieter <rdieter@fedoraproject.org> - 5.71.0-1
- 5.71.0

* Mon May 04 2020 Rex Dieter <rdieter@fedoraproject.org> - 5.70.0-1
- 5.70.0

* Tue Apr 21 2020 Rex Dieter <rdieter@fedoraproject.org> - 5.69.0-1
- 5.69.0

* Fri Mar 20 2020 Rex Dieter <rdieter@fedoraproject.org> - 5.68.0-1
- 5.68.0

* Mon Feb 03 2020 Rex Dieter <rdieter@fedoraproject.org> - 5.67.0-1
- 5.67.0

* Wed Jan 29 2020 Fedora Release Engineering <releng@fedoraproject.org> - 5.66.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_32_Mass_Rebuild

* Tue Jan 07 2020 Rex Dieter <rdieter@fedoraproject.org> - 5.66.0-1
- 5.66.0

* Tue Dec 17 2019 Rex Dieter <rdieter@fedoraproject.org> - 5.65.0-1
- 5.65.0

* Fri Nov 08 2019 Rex Dieter <rdieter@fedoraproject.org> - 5.64.0-1
- 5.64.0

* Tue Oct 22 2019 Rex Dieter <rdieter@fedoraproject.org> - 5.63.0-1
- 5.63.0

* Mon Sep 16 2019 Rex Dieter <rdieter@fedoraproject.org> - 5.62.0-1
- 5.62.0

* Wed Aug 07 2019 Rex Dieter <rdieter@fedoraproject.org> - 5.61.0-1
- 5.61.0

* Thu Jul 25 2019 Fedora Release Engineering <releng@fedoraproject.org> - 5.60.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_31_Mass_Rebuild

* Sat Jul 13 2019 Rex Dieter <rdieter@fedoraproject.org> - 5.60.0-1
- 5.60.0

* Thu Jun 06 2019 Rex Dieter <rdieter@fedoraproject.org> - 5.59.0-1
- 5.59.0

* Tue May 07 2019 Rex Dieter <rdieter@fedoraproject.org> - 5.58.0-1
- 5.58.0

* Tue Apr 09 2019 Rex Dieter <rdieter@fedoraproject.org> - 5.57.0-1
- 5.57.0

* Tue Mar 05 2019 Rex Dieter <rdieter@fedoraproject.org> - 5.56.0-1
- 5.56.0

* Mon Feb 04 2019 Rex Dieter <rdieter@fedoraproject.org> - 5.55.0-1
- 5.55.0

* Fri Feb 01 2019 Fedora Release Engineering <releng@fedoraproject.org> - 5.54.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_30_Mass_Rebuild

* Wed Jan 09 2019 Rex Dieter <rdieter@fedoraproject.org> - 5.54.0-1
- 5.54.0

* Sun Dec 09 2018 Rex Dieter <rdieter@fedoraproject.org> - 5.53.0-1
- 5.53.0

* Sun Nov 04 2018 Rex Dieter <rdieter@fedoraproject.org> - 5.52.0-1
- 5.52.0

* Wed Oct 10 2018 Rex Dieter <rdieter@fedoraproject.org> - 5.51.0-1
- 5.51.0

* Tue Sep 04 2018 Rex Dieter <rdieter@fedoraproject.org> - 5.50.0-1
- 5.50.0

* Tue Aug 07 2018 Rex Dieter <rdieter@fedoraproject.org> - 5.49.0-1
- 5.49.0

* Fri Jul 13 2018 Fedora Release Engineering <releng@fedoraproject.org> - 5.48.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Mon Jul 09 2018 Rex Dieter <rdieter@fedoraproject.org> - 5.48.0-1
- 5.48.0

* Fri Jun 08 2018 Rex Dieter <rdieter@fedoraproject.org> - 5.47.0-2
- cleanup, use %%majmin %%make_build %%ldconfig_scriptlets

* Sat Jun 02 2018 Rex Dieter <rdieter@fedoraproject.org> - 5.47.0-1
- 5.47.0

* Sat May 05 2018 Rex Dieter <rdieter@fedoraproject.org> - 5.46.0-1
- 5.46.0

* Sun Apr 08 2018 Rex Dieter <rdieter@fedoraproject.org> - 5.45.0-1
- 5.45.0

* Sat Mar 03 2018 Rex Dieter <rdieter@fedoraproject.org> - 5.44.0-1
- 5.44.0

* Wed Feb 07 2018 Rex Dieter <rdieter@fedoraproject.org> - 5.43.0-1
- 5.43.0

* Mon Jan 08 2018 Rex Dieter <rdieter@fedoraproject.org> - 5.42.0-1
- 5.42.0

* Mon Dec 04 2017 Rex Dieter <rdieter@fedoraproject.org> - 5.41.0-1
- 5.41.0

* Fri Nov 10 2017 Rex Dieter <rdieter@fedoraproject.org> - 5.40.0-1
- 5.40.0

* Sun Oct 08 2017 Rex Dieter <rdieter@fedoraproject.org> - 5.39.0-1
- 5.39.0

* Mon Sep 11 2017 Rex Dieter <rdieter@fedoraproject.org> - 5.38.0-1
- 5.38.0

* Fri Aug 25 2017 Rex Dieter <rdieter@fedoraproject.org> - 5.37.0-1
- 5.37.0

* Thu Aug 03 2017 Fedora Release Engineering <releng@fedoraproject.org> - 5.36.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Wed Jul 26 2017 Fedora Release Engineering <releng@fedoraproject.org> - 5.36.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Mon Jul 03 2017 Rex Dieter <rdieter@fedoraproject.org> - 5.36.0-1
- 5.36.0

* Sun Jun 04 2017 Rex Dieter <rdieter@fedoraproject.org> - 5.35.0-1
- 5.35.0

* Mon May 15 2017 Rex Dieter <rdieter@fedoraproject.org> - 5.34.0-1
- 5.34.0

* Mon Apr 03 2017 Rex Dieter <rdieter@fedoraproject.org> - 5.33.0-1
- 5.33.0

* Sat Mar 04 2017 Rex Dieter <rdieter@fedoraproject.org> - 5.32.0-1
- 5.32.0

* Mon Feb 06 2017 Rex Dieter <rdieter@fedoraproject.org> - 5.31.0-1
- 5.31.0

* Fri Dec 16 2016 Rex Dieter <rdieter@fedoraproject.org> - 5.29.0-1
- 5.29.0

* Tue Oct 04 2016 Rex Dieter <rdieter@fedoraproject.org> - 5.27.0-1
- 5.27.0

* Wed Sep 07 2016 Daniel Vrátil <dvratil@fedoraproject.org> - 5.26.0-1
- KDE Frameworks 6.26.0

* Mon Aug 08 2016 Daniel Vrátil <dvratil@fedoraproject.org> - 5.25.0-1
- KDE Frameworks 6.25.0

* Wed Jul 06 2016 Daniel Vrátil <dvratil@fedoraproject.org> - 5.24.0-1
- KDE Frameworks 6.24.0

* Tue Jun 07 2016 Daniel Vrátil <dvratil@fedoraproject.org> - 5.23.0-1
- KDE Frameworks 6.23.0

* Tue Jun 07 2016 Rex Dieter <rdieter@fedoraproject.org> - 5.22.0-2
- update URL

* Mon May 16 2016 Rex Dieter <rdieter@fedoraproject.org> - 5.22.0-1
- KDE Frameworks 6.22.0

* Mon Apr 04 2016 Rex Dieter <rdieter@fedoraproject.org> - 5.21.0-1
- KDE Frameworks 6.21.0

* Mon Mar 14 2016 Daniel Vrátil <dvratil@fedoraproject.org> - 5.20.0-1
- KDE Frameworks 6.20.0

* Mon Mar 07 2016 Rex Dieter <rdieter@fedoraproject.org> 5.19.0-2
- backport ktraderclient5 fix for x-scheme-handler's (#1308384,kde#358159)

* Thu Feb 11 2016 Daniel Vrátil <dvratil@fedoraproject.org> - 5.19.0-1
- KDE Frameworks 6.19.0

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 5.18.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Thu Jan 14 2016 Rex Dieter <rdieter@fedoraproject.org> 5.18.0-2
- cosmetics, -BR: cmake, update URL

* Sun Jan 03 2016 Daniel Vrátil <dvratil@fedoraproject.org> - 5.18.0-1
- KDE Frameworks 6.18.0

* Wed Dec 09 2015 Rex Dieter <rdieter@fedoraproject.org> 5.17.0-2
- reduce kbuildsycoca5 warning(spam) output

* Tue Dec 08 2015 Daniel Vrátil <dvratil@fedoraproject.org> - 5.17.0-1
- KDE Frameworks 6.17.0

* Sun Nov 08 2015 Daniel Vrátil <dvratil@fedoraproject.org> - 5.16.0-1
- KDE Frameworks 6.16.0

* Tue Oct 27 2015 Rex Dieter <rdieter@fedoraproject.org> 5.15.0-2
- backport 'kbuildsysoca always save' commit

* Thu Oct 08 2015 Daniel Vrátil <dvratil@redhat.com> - 5.15.0-1
- KDE Frameworks 6.15.0

* Thu Oct 08 2015 Rex Dieter <rdieter@fedoraproject.org> 5.14.3-3
- restore xdg-menu patch

* Wed Oct 07 2015 Rex Dieter <rdieter@fedoraproject.org> 5.14.3-2
- .spec cosmetics, update URL/License, use %%license

* Tue Sep 22 2015 Rex Dieter <rdieter@fedoraproject.org> 5.14.3-1
- kservice-5.14.3

* Wed Sep 16 2015 Daniel Vrátil <dvratil@redhat.com> - 5.14.0-1
- KDE Frameworks 6.14.0

* Wed Aug 19 2015 Daniel Vrátil <dvratil@redhat.com> - 5.13.0-1
- KDE Frameworks 6.13.0

* Wed Aug 19 2015 Daniel Vrátil <dvratil@redhat.com> - 5.13.0-1
- KDE Frameworks 6.13.0

* Tue Aug 11 2015 Daniel Vrátil <dvratil@redhat.com> - 5.13.0-0.1
- KDE Frameworks 6.13

* Fri Jul 10 2015 Rex Dieter <rdieter@fedoraproject.org> - 5.12.0-1
- 5.12.0

* Wed Jun 17 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 5.11.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Wed Jun 10 2015 Daniel Vrátil <dvratil@redhat.com> - 5.11.0-1
- KDE Frameworks 6.11.0

* Wed May 20 2015 Rex Dieter <rdieter@fedoraproject.org> 5.10.0-4
- drop no_debug patch, fixed better elsewhere (qt-settings)

* Tue May 19 2015 Rex Dieter <rdieter@fedoraproject.org> 5.10.0-3
- use %%find_lang --with-man for localized manpages

* Tue May 19 2015 Rex Dieter <rdieter@fedoraproject.org> 5.10.0-2
- .spec cosmetics, disable debug output

* Mon May 11 2015 Daniel Vrátil <dvratil@redhat.com> - 5.10.0-1
- KDE Frameworks 6.10.0

* Sat May 02 2015 Kalev Lember <kalevlember@gmail.com> - 5.9.0-2
- Rebuilt for GCC 5 C++11 ABI change

* Tue Apr 07 2015 Daniel Vrátil <dvratil@redhat.com> - 5.9.0-1
- KDE Frameworks 6.9.0

* Mon Mar 16 2015 Daniel Vrátil <dvratil@redhat.com> - 5.8.0-1
- KDE Frameworks 6.8.0

* Fri Feb 27 2015 Daniel Vrátil <dvratil@redhat.com> - 5.7.0-2
- Rebuild (GCC 5)

* Mon Feb 16 2015 Daniel Vrátil <dvratil@redhat.com> - 5.7.0-1
- KDE Frameworks 6.7.0

* Mon Feb 09 2015 Daniel Vrátil <dvratil@redhat.com> - 5.7.0-1
- KDE Frameworks 6.7.0

* Thu Jan 08 2015 Daniel Vrátil <dvratil@redhat.com> - 5.6.0-1
- KDE Frameworks 6.6.0

* Mon Dec 08 2014 Daniel Vrátil <dvratil@redhat.com> - 5.5.0-1
- KDE Frameworks 6.5.0

* Mon Nov 03 2014 Daniel Vrátil <dvratil@redhat.com> - 5.4.0-1
- KDE Frameworks 6.4.0

* Tue Oct 07 2014 Daniel Vrátil <dvratil@redhat.com> - 5.3.0-1
- KDE Frameworks 6.3.0

* Mon Sep 15 2014 Daniel Vrátil <dvratil@redhat.com> - 5.2.0-1
- KDE Frameworks 6.2.0

* Sat Aug 16 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 5.1.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Wed Aug 06 2014 Daniel Vrátil <dvratil@redhat.com> - 5.1.0-1
- KDE Frameworks 6.1.0

* Wed Jul 09 2014 Daniel Vrátil <dvratil@redhat.com> - 5.0.0-1
- KDE Frameworks 6.0.0

* Tue Jun 03 2014 Daniel Vrátil <dvratil@redhat.com> - 4.100.0-1
- KDE Frameworks 4.100.0

* Mon May 05 2014 Daniel Vrátil <dvratil@redhat.com> - 4.99.0
- KDE Frameworks 4.99.0

* Mon Mar 31 2014 Jan Grulich <jgrulich@redhat.com> 4.98.0-1
- Update to KDE Frameworks 6 Beta 1 (4.98.0)

* Wed Mar 05 2014 Jan Grulich <jgrulich@redhat.com> 4.97.0-1
- Update to KDE Frameworks 6 Alpha 1 (4.97.0)

* Wed Feb 12 2014 Daniel Vrátil <dvratil@redhat.com> 4.96.0-1
- Update to KDE Frameworks 6 Alpha 1 (4.96.0)

* Wed Feb 05 2014 Daniel Vrátil <dvratil@redhat.com> 4.96.0-0.1.20140205git
- Update to pre-relase snapshot of 4.96.0

* Thu Jan 16 2014 Daniel Vrátil <dvratil@redhat.com> 4.95.0-2
- rebuild against new kf6-filesystem

* Thu Jan 09 2014 Daniel Vrátil <dvratil@redhat.com> 4.95.0-1
- Update to KDE Frameworks 6 TP1 (4.95.0)

* Sat Jan  4 2014 Daniel Vrátil <dvratil@redhat.com>
- initial version
