%global gitdate 20230915.155315
%global cmakever 5.240.0
%global commit0 453e354b88d0fd17da12c72802c199fbd29d5d66
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global date 20221108

%global orig_name kirigami-addons
%global framework kirigami-addons

Name:           kf6-kirigami2-addons
Version:        %{cmakever}^%{gitdate}.%{shortcommit0}
Release:        129%{?dist}
Epoch:          1
License:        LGPLv3
Summary:        Convergent visual components ("widgets") for Kirigami-based applications
Url:            https://invent.kde.org/libraries/kirigami-addons
Source:         kirigami-addons-%{shortcommit0}.tar.gz

BuildRequires: cmake
BuildRequires: extra-cmake-modules
BuildRequires: gcc-c++
BuildRequires: kf6-rpm-macros
BuildRequires:  cmake(KF6I18n)
BuildRequires:  cmake(KF6Kirigami2)
BuildRequires:  cmake(KF6I18n)
BuildRequires:  cmake(KF6Kirigami2)

BuildRequires:  cmake(Qt6Core)
BuildRequires:  cmake(Qt6Quick)
BuildRequires:  cmake(Qt6QuickControls2)

BuildRequires:  pkgconfig(xkbcommon)

%description
A set of "widgets" i.e visual end user components along with a
code to support them. Components are usable by both touch and
desktop experiences providing a native experience on both, and
look native with any QQC2 style (qqc2-desktop-theme, Material
or Plasma).

%package dateandtime
Summary:        Date and time add-on for the Kirigami framework
Requires:       %{name} = %{epoch}:%{version}-%{release}

%description dateandtime
Date and time Kirigami addons, which complements other
software like Kclock.

%package treeview
Summary:         Tree view add-on for the Kirigami framework
Requires:        %{name} = %{epoch}:%{version}-%{release}
%description treeview
Tree view Kirigami addon, which is useful for listing files.

%prep
%autosetup -n %{orig_name}-%{shortcommit0}

%build
%cmake_kf6 -DBUILD_WITH_QT6=ON  -DQT_MAJOR_VERSION=6
%cmake_build

%install
%cmake_install
%find_lang %{orig_name} --all-name

%files -f %{orig_name}.lang
%doc README.md
%license LICENSES/
%dir %{_kf6_qmldir}/org/kde
%dir %{_kf6_qmldir}/org/kde/kirigamiaddons
%{_kf6_libdir}/qt6/qml/org/kde/kirigamiaddons/*
%{_kf6_libdir}/cmake/KF6KirigamiAddons/*


%files dateandtime
%{_kf6_qmldir}/org/kde/kirigamiaddons/dateandtime/

%files treeview
%{_kf6_qmldir}/org/kde/kirigamiaddons/treeview/

%changelog
* Tue Sep 19 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230915.155315.453e354-129
- rebuilt

* Mon Sep 18 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230915.155315.453e354-128
- rebuilt

* Mon Sep 18 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230915.155315.453e354-127
- rebuilt

* Mon Sep 18 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230915.155315.453e354-126
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230913.131034.70a4022-125
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230913.131034.70a4022-124
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230913.131034.70a4022-123
- rebuilt

* Fri Sep 15 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230913.131034.70a4022-122
- rebuilt

* Thu Sep 14 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230911.150542.6e093eb-121
- rebuilt

* Tue Sep 12 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230911.150542.6e093eb-120
- rebuilt

* Tue Sep 12 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230909.164631.24eb3cf-119
- rebuilt

* Mon Sep 11 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230909.164631.24eb3cf-118
- rebuilt

* Sun Sep 10 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230909.164631.24eb3cf-117
- rebuilt

* Sun Sep 10 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230905.105120.54f871f-116
- rebuilt

* Sat Sep 09 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230905.105120.54f871f-115
- rebuilt

* Sat Sep 09 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230905.105120.54f871f-114
- rebuilt

* Thu Sep 07 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230905.105120.54f871f-113
- rebuilt

* Thu Sep 07 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230905.105120.54f871f-112
- rebuilt

* Thu Sep 07 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230905.105120.54f871f-111
- rebuilt

* Wed Sep 06 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230905.105120.54f871f-110
- rebuilt

* Wed Sep 06 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230831.161351.6933361-109
- rebuilt

* Tue Sep 05 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230831.161351.6933361-108
- rebuilt

* Mon Sep 04 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230831.161351.6933361-107
- rebuilt

* Sun Sep 03 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230831.161351.6933361-106
- rebuilt

* Sat Sep 02 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230831.161351.6933361-105
- rebuilt

* Fri Sep 01 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230830.104732.3de0aae-104
- rebuilt

* Thu Aug 31 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230830.104732.3de0aae-103
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230828.205117.92cf8f5-102
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230828.205117.92cf8f5-101
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230828.205117.92cf8f5-100
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230828.205117.92cf8f5-99
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230828.205117.92cf8f5-98
- rebuilt

* Wed Aug 30 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230828.205117.92cf8f5-97
- rebuilt

* Tue Aug 29 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230827.160147.dc6e1ea-96
- rebuilt

* Mon Aug 28 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230824.195122.f3fa24e-95
- rebuilt

* Sun Aug 27 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230824.195122.f3fa24e-94
- rebuilt

* Sat Aug 26 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230824.195122.f3fa24e-93
- rebuilt

* Sat Aug 26 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230824.195122.f3fa24e-92
- rebuilt

* Fri Aug 25 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230823.190817.6ed4382-91
- rebuilt

* Thu Aug 24 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230822.121023.7284115-90
- rebuilt

* Wed Aug 23 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230821.103233.4b9965d-89
- rebuilt

* Tue Aug 22 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230821.103233.4b9965d-88
- rebuilt

* Mon Aug 21 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230820.163340.29ad3e0-87
- rebuilt

* Mon Aug 21 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230819.155052.0c52e7d-86
- rebuilt

* Sun Aug 20 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230818.150203.1aca087-85
- rebuilt

* Sat Aug 19 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230817.003619.2d31bcc-84
- rebuilt

* Fri Aug 18 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230817.003619.2d31bcc-83
- rebuilt

* Thu Aug 17 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230815.224844.044c241-82
- rebuilt

* Wed Aug 16 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230815.224844.044c241-81
- rebuilt

* Tue Aug 15 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230814.220722.4670f52-80
- rebuilt

* Tue Aug 15 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230814.220722.4670f52-79
- rebuilt

* Tue Aug 15 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230813.113246.2ac583d-78
- rebuilt

* Sun Aug 13 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230812.113751.c1ac225-77
- rebuilt

* Sun Aug 13 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230809.140653.a0562a6-76
- rebuilt

* Fri Aug 11 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230809.004244.a0562a6-75
- rebuilt

* Thu Aug 10 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230809.004244.a0562a6-74
- rebuilt

* Thu Aug 10 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230809.004244.a0562a6-73
- rebuilt

* Thu Aug 10 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230809.004558.12a3a5a-72
- rebuilt

* Wed Aug 09 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230807.224105.a8710f5-71
- rebuilt

* Tue Aug 08 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230805.163421.2eb6ec2-70
- rebuilt

* Mon Aug 07 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230805.163421.2eb6ec2-69
- rebuilt

* Sun Aug 06 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230805.163421.2eb6ec2-68
- rebuilt

* Sun Aug 06 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230805.163421.2eb6ec2-67
- rebuilt

* Sun Aug 06 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230802.193238.4740fad-66
- rebuilt

* Sat Aug 05 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230802.193238.4740fad-65
- rebuilt

* Fri Aug 04 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230802.193238.4740fad-64
- rebuilt

* Fri Aug 04 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230802.224359.082dc8c-63
- rebuilt

* Thu Aug 03 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230730.212335.567ac96-62
- rebuilt

* Wed Aug 02 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230730.212335.567ac96-61
- rebuilt

* Wed Aug 02 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230730.212335.567ac96-60
- rebuilt

* Tue Aug 01 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230730.212335.567ac96-59
- rebuilt

* Mon Jul 31 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230730.212335.567ac96-58
- rebuilt

* Mon Jul 31 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230730.212335.567ac96-57
- rebuilt

* Mon Jul 31 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230727.223952.3468d52-56
- rebuilt

* Sun Jul 30 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230727.223952.3468d52-55
- rebuilt

* Sun Jul 30 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230727.223952.3468d52-54
- rebuilt

* Sat Jul 29 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230727.223952.3468d52-53
- rebuilt

* Fri Jul 28 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230727.223952.3468d52-52
- rebuilt

* Fri Jul 28 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230720.234650.9c0848d-51
- rebuilt

* Thu Jul 27 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230720.234650.9c0848d-50
- rebuilt

* Wed Jul 26 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230720.234650.9c0848d-49
- rebuilt

* Tue Jul 25 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230720.234650.9c0848d-48
- rebuilt

* Mon Jul 24 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230720.234650.9c0848d-47
- rebuilt

* Mon Jul 24 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230719.222244.99eb012-46
- rebuilt

* Sat Jul 22 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230719.222244.99eb012-45
- rebuilt

* Fri Jul 21 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230719.222244.99eb012-44
- rebuilt

* Thu Jul 20 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230708.103924.1f91de7-43
- rebuilt

* Tue Jul 18 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230708.103924.1f91de7-42
- rebuilt

* Tue Jul 18 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230708.103924.1f91de7-41
- rebuilt

* Tue Jul 18 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230716.110455.8083785-40
- rebuilt

* Mon Jul 17 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230716.110455.8083785-39
- rebuilt

* Sun Jul 16 2023 Justin Zobel <justin@1707.io> - 1:5.240.0^20230710.135922.2bdaa04-38
- rebuilt

* Sun Jul 16 2023 Justin Zobel <justin@1707.io> - 1:5.240.0^20230710.135922.2bdaa04-37
- rebuilt

* Sat Jul 15 2023 Justin Zobel <justin@1707.io> - 1:5.240.0^20230710.135922.2bdaa04-36
- rebuilt

* Sat Jul 15 2023 Justin Zobel <justin@1707.io> - 1:5.240.0^20230710.135922.2bdaa04-35
- rebuilt

* Fri Jul 14 2023 Justin Zobel <justin@1707.io> - 1:5.240.0^20230710.135922.2bdaa04-34
- rebuilt

* Fri Jul 14 2023 Justin Zobel <justin@1707.io> - 1:5.240.0^20230710.135922.2bdaa04-33
- rebuilt

* Fri Jul 14 2023 Justin Zobel <justin@1707.io> - 1:5.240.0^20230710.135922.2bdaa04-32
- rebuilt

* Fri Jul 14 2023 Justin Zobel <justin@1707.io> - 1:5.240.0^20230710.135922.2bdaa04-31
- rebuilt

* Thu Jul 13 2023 Justin Zobel <justin@1707.io> - 1:5.240.0^20230710.135922.2bdaa04-30
- rebuilt

* Thu Jul 13 2023 Justin Zobel <justin@1707.io> - 1:5.240.0^20230710.135922.2bdaa04-29
- rebuilt

* Thu Jul 13 2023 Justin Zobel <justin@1707.io> - 1:5.240.0^20230708.125137.1cbebd1-28
- rebuilt

* Mon Jul 10 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230708.125137.1cbebd1-27
- rebuilt

* Sun Jul 09 2023 Justin Zobel <justin@1707.io> - 1:5.240.0^20230708.104658.4f69bd6-26
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin@1707.io> - 1:5.240.0^20230702.112510.b19d0e2-25
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230702.112510.b19d0e2-24
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230702.112510.b19d0e2-23
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230702.112510.b19d0e2-22
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230702.112510.b19d0e2-21
- rebuilt

* Sat Jul 08 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230702.112510.b19d0e2-20
- rebuilt

* Thu Jul 06 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230705.195108.620b77c-19
- rebuilt

* Wed Jul 05 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230630.131450.5bd1c2d-18
- rebuilt

* Tue Jul 04 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230630.131450.5bd1c2d-17
- rebuilt

* Sun Jul 02 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230630.131450.5bd1c2d-16
- rebuilt

* Sun Jul 02 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230630.131450.5bd1c2d-15
- rebuilt

* Sat Jul 01 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230628.161330.675809f-14
- rebuilt

* Thu Jun 29 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230620.172850.b4d1e77-13
- rebuilt

* Wed Jun 28 2023 Justin - 1:5.240.0^20230620.172850.b4d1e77-12
- rebuilt

* Tue Jun 27 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230624.221346.379d80a-11
- rebuilt

* Sun Jun 25 2023 Justin - 1:5.240.0^20230624.221346.379d80a-10
- rebuilt

* Sun Jun 25 2023 Justin - 1:5.240.0^20230624.221346.379d80a-9
- rebuilt

* Sun Jun 25 2023 Justin - 1:5.240.0^20230622.200024.039c215-8
- rebuilt

* Fri Jun 23 2023 Justin Zobel <justin.zobel@gmail.com> - 1:5.240.0^20230601.015121.474caad-7
- rebuilt

* Fri Jun 02 2023 justin - 1:5.240.0^20230601.015121.474caad-6
- rebuilt

* Fri Jun 02 2023 justin - 1:5.240.0^20230601.015121.2baf0a4-5
- rebuilt

* Fri Jun 02 2023 justin - 1:5.240.0^20230531.021503.9159cb2-4
- rebuilt

* Wed May 31 2023 justin - 1:5.240.0^20230531.021503.9159cb2-3
- rebuilt

* Wed May 31 2023 justin - 1:5.240.0^20230530.020219.9dde83c-2
- rebuilt

* Thu Jan 19 2023 Fedora Release Engineering <releng@fedoraproject.org> - 1:0.6-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_38_Mass_Rebuild

* Wed Nov 30 2022 Marc Deop <marcdeop@fedoraproject.org> - 1:0.6-1
- 0.6

* Wed Sep 28 2022 Justin Zobel <justin@1707.io> - 0.4-1
- Update to 0.4

* Thu Jul 21 2022 Fedora Release Engineering <releng@fedoraproject.org> - 21.05-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_37_Mass_Rebuild

* Thu Jul 14 2022 Jan Grulich <jgrulich@redhat.com> - 21.05-6
- Rebuild (qt6)

* Tue May 17 2022 Jan Grulich <jgrulich@redhat.com> - 21.05-5
- Rebuild (qt6)

* Fri Mar 11 2022 Jan Grulich <jgrulich@redhat.com> - 21.05-4
- Rebuild (qt6)

* Thu Jan 20 2022 Fedora Release Engineering <releng@fedoraproject.org> - 21.05-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_36_Mass_Rebuild

* Thu Jul 22 2021 Fedora Release Engineering <releng@fedoraproject.org> - 21.05-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_35_Mass_Rebuild

* Sat May 15 2021 Onuralp SEZER <thunderbirdtr@fedoraproject.org> - 21.05-1
- initial version of package
